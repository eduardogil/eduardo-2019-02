import '../css/cabecalho.css';
import Logo from '../img/Logo.jpg';

const Cabecalho = props => {
  return (
    <React.Fragment>
      <div className="header">
        <img className="logo" src={Logo} alt=""/>
        <input className="input" type="text" id="Pesquisar"/>
      </div>
    </React.Fragment>
  )
}

export default Cabecalho;