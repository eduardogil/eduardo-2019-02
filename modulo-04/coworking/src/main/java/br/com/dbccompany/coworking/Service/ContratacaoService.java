package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Contratacao salvar(Contratacao contratacao ) {
        return contratacaoRepository.save( contratacao );
    }

    @Transactional( rollbackFor = Exception.class )
    public Contratacao editar( Integer id, Contratacao contratacao ) {
        contratacao.setId( id );
        return contratacaoRepository.save( contratacao );
    }

    public List<Contratacao> todasContratacao() {
        return contratacaoRepository.findAll();
    }
}