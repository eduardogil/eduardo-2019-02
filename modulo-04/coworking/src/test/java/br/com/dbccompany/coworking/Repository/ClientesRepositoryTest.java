package br.com.dbccompany.coworking.Repository;


import static org.assertj.core.api.Assertions.assertThat;
import br.com.dbccompany.coworking.Entity.Clientes;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ClientesRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ClientesRepository clientesRepository;

    @Test
    public void procurarClientesPorNome(){

        Clientes clientes = new Clientes();
        clientes.setNome("Eduardo");
        clientes.setCpf("02467413098");
        entityManager.persist(clientes);
        entityManager.flush();

        Clientes resposta = clientesRepository.findByNome(clientes.getNome());
        assertThat(resposta.getNome()).isEqualTo(clientes.getNome());
    }
}
