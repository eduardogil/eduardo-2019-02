import React, { Component } from 'react';
import * as axios from 'axios';

import '../styles/header.css';
import '../styles/login.css';

export default class Login extends Component {
    constructor(props){
        super(props)
        this.state = {
            username: '',
            password: ''
        }
        this.trocaValoresState = this.trocaValoresState.bind( this )
    }

    trocaValoresState(e){
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }
    
    logar(e){
        e.preventDefault();

        const { username, password } = this.state
        console.log(username, password)
        if (username && password){
            axios.post('http://localhost:8080/login',{
                username: this.state.username,
                password: this.state.password
            }).then( resp =>{
                console.log(resp)
                localStorage.setItem( 'Authorization', resp.headers.authorization );
                this.props.history.push("/home")
                }
            )
        }
    }

    render(){
        return (
            <React.Fragment>
                <div className="display">
                    <h2>LOGAR</h2>
                    <input type="text" name="username" id="username" placeholder="Digite seu username" onChange={ this.trocaValoresState }/>
                    <input type="password" name="password" id="password" placeholder="Digite seu password" onChange={ this.trocaValoresState }/>
                    <button id="login" onClick={ this.logar.bind( this ) }>Logar</button>

                </div>
            </React.Fragment>
        )
    }

}