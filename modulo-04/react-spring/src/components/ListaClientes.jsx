import React, { Component } from 'react';
import Axios from 'axios';

class ListaClientes extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listaClientes:[]
        }
    }
    componentWillMount() {
        Axios.get('http://localhost:8080/elfos', {
            headers: {
                authorization: localStorage.getItem('Authorization')
            }
        }).then(resp => {
            this.setState({
                listaClientes: resp.data.clientes
                })
            })
        }
}

export default ListaClientes;