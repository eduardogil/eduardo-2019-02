import React from 'react';

const ListaDeAvaliacoes = props => {

    
    const{ episodio } = props
    return (
        <React.Fragment>
                {episodio.assitido} ? (
                <div>
                <h2>{ episodio.nome }</h2>
                {/* <img className="folder" src={ episodio.thumbUrl } alt={ episodio.nome } width="50" height="50"/> */}
                <h4>Nota Atual : { episodio.nota || '' }</h4>
                </div>) : (
                    <div>
                    </div>
                )
        </React.Fragment>
    )
}

export default ListaDeAvaliacoes;