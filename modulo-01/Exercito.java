import java.util.*;
public class Exercito
{
    private ArrayList<Elfo> elfos = new ArrayList<Elfo>();
    private HashMap<Status, ArrayList<Elfo>> porStatus  = new HashMap<>();
    private final ArrayList<Class> TIPOS_PERMITIDOS
    = new ArrayList<>(Arrays.asList(ElfoVerde.class, ElfoNoturno.class));   

    public ArrayList<Elfo> noturnosPorUltimo(){
        ArrayList<Elfo> formacao = new ArrayList<Elfo>();       
        for(Elfo elf : elfos){
            if(isVerde(elf) &&  isVivo(elf)){
                formacao.add(elf);
            }
        }
        for(Elfo elf : elfos){
            if(isNoturno(elf) && isVivo(elf)){
                formacao.add(elf);
            }
        }
        return formacao;
    }

    private boolean isVivo(Elfo e){
        return e.getStatus() != Status.MORTO;
    }

    private ArrayList<Elfo> getListaSemMortos(){
        ArrayList<Elfo> vivos = new ArrayList<Elfo>();
        for(Elfo ef : elfos){
            if(isVivo(ef)){
                vivos.add(ef);
            }
        }
        return vivos;
    }

    private ArrayList<Elfo> cortaElfosEmDemasia(ArrayList<Elfo> listaElfica){
        int verde = 0;
        int noturno = 0;
        ArrayList<Elfo> novaList = new ArrayList<Elfo>(listaElfica);
        for(Elfo ef : listaElfica){
            if(isNoturno(ef)){
                noturno++;
            }else{
                verde++;
            }
        }
        int count = 0;
        if(verde > noturno){
            count = verde - noturno;
            for(Elfo ef : listaElfica){
                if(count > 0){
                    if(isVerde(ef)){
                        count--;
                        novaList.remove(ef);
                    }
                }else{break;}
            }
        }else{
            count = noturno - verde;
            for(Elfo ef : listaElfica){
                if(count > 0){
                    if(isNoturno(ef)){
                        count--;
                        novaList.remove(ef);
                    }
                }else{
                    break;
                }
            }
        }
        return novaList;
    }

    public ArrayList<Elfo> ataqueIntercalado(){        
        ArrayList<Elfo> listaElfosLimpa = cortaElfosEmDemasia(getListaSemMortos());
        ArrayList<Elfo> formacao = new ArrayList<Elfo>();
        boolean verde = false;
        boolean noturno = false;
        while(formacao.size() < listaElfosLimpa.size()){
            for(Elfo elf : listaElfosLimpa){ 
                if(isNoturno(elf)){
                    if(!noturno && !formacao.contains(elf)){
                        verde = false;
                        noturno = true;                        
                        formacao.add(elf);                   
                    }
                }else{
                    if(!verde && !formacao.contains(elf)){
                        verde = true;
                        noturno = false;
                        formacao.add(elf);                    
                    }
                }
            }
        }
        return formacao;
    }    
    
    private boolean isNoturno(Elfo elfo){
        return elfo.getClass() == ElfoNoturno.class;   
    }

    private boolean isVerde(Elfo elfo){
        return elfo.getClass() == ElfoVerde.class;   
    }

    private boolean podeEntrarExercito(Elfo elf){
        return TIPOS_PERMITIDOS.contains(elf.getClass());
    }

    public ArrayList<Elfo> getExercito(){
        return elfos;
    }

        public void alistar(Elfo elf){
        if(podeEntrarExercito(elf)){
            elfos.add(elf);
            ArrayList<Elfo> elfoDoStatus = porStatus.get(elf.getStatus());
            if(elfoDoStatus == null){
                elfoDoStatus = new ArrayList<>();
                porStatus.put(elf.getStatus(), elfoDoStatus);
            }
            elfoDoStatus.add(elf);
        }
    }

    public ArrayList<Elfo> getElfosPorStatus(Status status){
        return porStatus.get(status);                                                       
    }

    public void atacar(ArrayList<Elfo> formacao, Dwarf dwarf){
        for(Elfo elfo : formacao){
            elfo.atacarComFlecha(dwarf);
        }
    }                 
                
    public void atacarComFlecha(){
    }
}