import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../css/MeuInputNumeroExemplo.css';
import MensagemFlashConst from './MensagemFlashConst';

export default class MeuInputNumero extends Component {
    
    perderFoco = evt => {
        const { obrigatorio, atualizarValor} = this.props
        const valor = evt.target.value
        const erro = obrigatorio && !valor
        atualizarValor( { valor, erro } )
    }

    render(){
        const { placeholder, mensagemCampo, visivel, deveExibirErro } = this.props
        return visivel ? (
            <React.Fragment>
                {
                    mensagemCampo && <span>{ mensagemCampo }</span>
                }
                {
                    <input type="number" className={ deveExibirErro ? 'erro' : '' } placeholder = { placeholder } onBlur={ this.perderFoco }/>
                }
                {
                    deveExibirErro && <span className="mensagem-erro">{ MensagemFlashConst.ERRO.CAMPO_OBRIGATORIO }</span>
                }
            </React.Fragment>
        ) : null
    }
}

MeuInputNumero.propTypes = {
    visivel: PropTypes.bool.isRequired,
    deveExibirErro: PropTypes.bool.isRequired,
    placeholder: PropTypes.string,
    mensagemCampo: PropTypes.string,
    obrigatorio: PropTypes.bool
}

MeuInputNumero.defaultProps = {
    obrigatorio: false
}