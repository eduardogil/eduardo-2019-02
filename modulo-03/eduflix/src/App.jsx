import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import InfoJsFlix from './InfoJsFlix';
import BlackMirror from './BlackMirror';
import Home from './Home';
import './css/App.css';
import Login from './Login';
import { PrivateRoute } from './components/PrivateRoute';

export default class App extends Component {
  render(){
    return(
      <div className="App">
        <Router>
          <React.Fragment>
            <section>
              <PrivateRoute path="/" exact component={ Home } />
              <Route path="/login" exact component={ Login } />
              <Route path="/InjoJsFlix" component={ InfoJsFlix } />
              <Route path="/BlackMirror" component={ BlackMirror } />
            </section>
          </React.Fragment>
        </Router>
      </div>
    );
  }
  
}

const JsFlix = () => <h2>DBC PLAY!</h2>
const Mirror = () => <h2>Black Mirror</h2>