package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Correntistas;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class CorrentistasDAO extends AbstractDAO<Correntistas>{

private static final EnderecosDAO ENDERECOS_DAO = new EnderecosDAO(); 
	
	public Correntistas parseFrom(CorrentistasDTO dto) {
		Correntistas correntistas = null;
		if (dto.getIdCorrentista() !=null) {
			correntistas = buscar(dto.getIdCorrentista());
		}else {
			correntistas = new Correntistas();
		}

		correntistas.setCnpj(dto.getCnpj());
		correntistas.setRazaoSocial(dto.getRazaoSocial());
		correntistas.setSaldo(dto.getSaldo());
		return correntistas;
	}
	@Override
	protected Class<Correntistas> getEntityClass() {
		return Correntistas.class;
	}

}
