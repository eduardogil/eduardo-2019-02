package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TipoContato")
public class TipoContato extends AbstractEntity{

    @Id
    @SequenceGenerator(allocationSize = 1, name = "TIPOCONTATO_SEQ", sequenceName = "TIPOCONTATO_SEQ")
    @GeneratedValue(generator = "TIPOCONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_TIPOCONTATO")
    private Integer id;

    @Column(name = "NOME", length = 100, nullable = false)
    private String nome;

    @OneToMany(mappedBy = "tipoContato", cascade = CascadeType.MERGE)
    private List<Contatos> contatos = new ArrayList<>();

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public List<Contatos> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contatos> contatos) {
        this.contatos = contatos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


}