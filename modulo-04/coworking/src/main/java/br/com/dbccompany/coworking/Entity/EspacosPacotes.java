package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Enum.TipoContratacao;

import javax.persistence.*;

@Entity
public class EspacosPacotes extends AbstractEntity{
    @Id
    @Column(name = "ID_ESPACO_PACOTE")
    @SequenceGenerator(allocationSize = 1, name = "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ")
    @GeneratedValue(generator = "ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "QUANTIDADE")
    private Integer quantidade;

    @Column(name = "PRAZO")
    private Integer prazo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_ESPACOS")
    private Espacos espacos;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_PACOTES")
    private Pacotes pacotes;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }
    public Espacos getEspaco() {
        return espacos;
    }

    public void setEspaco(Espacos espacos) {
        this.espacos = espacos;
    }

    public Pacotes getPacote() {
        return pacotes;
    }

    public void setPacote(Pacotes pacotes) {
        this.pacotes = pacotes;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}