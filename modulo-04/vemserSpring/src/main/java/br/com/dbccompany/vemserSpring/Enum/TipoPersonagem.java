package br.com.dbccompany.vemserSpring.Enum;

public enum TipoPersonagem {
    ELFO, ELFO_NOTURNO, NAO_DEFINIDO, ELFO_VERDE, ELFO_DA_LUZ, DWARF, DWARF_BARBA_LONGA
}
