package br.com.dbccompany.vemserSpring.Entity;

import br.com.dbccompany.vemserSpring.Enum.TipoOrdenacao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
public class Inventario {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "INVENTARIO_SEQ", sequenceName = "INVENTARIO_SEQ")
    @GeneratedValue(generator = "INVENTARIO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID_INVENTARIO" )
    private Integer id;

    @Column( name = "tamanho",  length = 100, nullable = false)
    private Integer tamanho;

    @OneToOne(cascade=CascadeType.ALL)
    private Personagem personagem;

    @OneToMany( mappedBy = "inventario")
    private List<InventarioXItem> lstInventarioItem;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTamanho() {
        return tamanho;
    }

    public void setTamanho(Integer tamanho) {
        this.tamanho = tamanho;
    }

    public List<InventarioXItem> getLstInventarioItem() {
        return lstInventarioItem;
    }

    public void setLstInventarioItem(List<InventarioXItem> lstInventarioItem) {
        this.lstInventarioItem = lstInventarioItem;
    }

}
