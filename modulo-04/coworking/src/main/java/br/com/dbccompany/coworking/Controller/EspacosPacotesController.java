package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.EspacosPacotes;
import br.com.dbccompany.coworking.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacospacotes")
public class EspacosPacotesController {

    @Autowired
    EspacosPacotesService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<EspacosPacotes> todosEspacosPacotes() {
        return service.todosEspacosPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public EspacosPacotes novoEspacosPacotes(@RequestBody EspacosPacotes espacosPacotes) {
        return service.salvar(espacosPacotes);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public EspacosPacotes editarEspacosClientes(@PathVariable Integer id, @RequestBody EspacosPacotes espacosPacotes){
        return service.editar(id, espacosPacotes);
    }
}
