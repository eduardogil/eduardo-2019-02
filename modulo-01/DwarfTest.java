import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest{
    
    private final double DELTA = 1e-9;
    
    @Test
    public void dwarfNasceCom110DeVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        assertEquals(110.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfPerder10DeVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        dwarf.sofrerDano();
        assertEquals(100.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfPerderVida22Ataques(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        for(int i=0 ; i < 22 ; i++){
            dwarf.sofrerDano();
        }
        assertEquals(0.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfNasceComStatus(){
        Dwarf dwarf = new Dwarf("Gimli");
        assertEquals(Status.RECEM_CRIADO,dwarf.getStatus());
    }
    
    @Test
    public void dwarfPerdeVidaEContinuaVivo(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.sofrerDano();
        assertEquals(Status.SOFREU_DANO,dwarf.getStatus());
    }
    
    
    @Test
    public void dwarfPerdeVidaEMorre(){
        Dwarf dwarf = new Dwarf("Gimli");
        for (int i=0; i<24; i++){
            dwarf.sofrerDano();
        }
        assertEquals(Status.MORTO, dwarf.getStatus());
        assertEquals(0.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfNasceComEscudoNoInvetario(){
        Dwarf dwarf = new Dwarf("Guardian");
        assertNotNull(dwarf.inventario.buscar("Escudo"));
    }
    
    @Test
    public void dwarfEquipaEscudoETomaMetadeDano(){
        Dwarf dwarf = new Dwarf("Guardian");
        dwarf.equiparEscudo();
        dwarf.sofrerDano();
        assertEquals(105.00,dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfNaoEquipaEscudoETomaDanoIntegral(){
        Dwarf dwarf = new Dwarf("Guardian");
        dwarf.desequipar();
        dwarf.sofrerDano();
        assertEquals(100.00,dwarf.getVida(), DELTA);
    }
}
