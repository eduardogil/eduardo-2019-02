public class Dwarf extends Personagem{
    private boolean escudoEquipado=false;
    
    public Dwarf(String nome){
        super(nome);
        this.inventario.adicionar(new Item(1,"Escudo"));
        this.vida = 110.0;
    }
    
    public boolean equiparEscudo(){
       if(this.inventario.buscar("Escudo")!=null){
            this.escudoEquipado=true;
       }
       return this.escudoEquipado;
    }
    
    public boolean desequipar(){
        return this.escudoEquipado=false;
    }
    
    @Override
    public double calcularDano(){
       return this.escudoEquipado ? 5.0 : this.qtdDano;
    }
    
    @Override
    public void sofrerDano(){
        if(this.podeSofrerDano()){
            this.vida = this.vida >= this.calcularDano() ? this.vida - this.calcularDano() : 0.0;
            this.status = status.SOFREU_DANO;
        }
        
        if(this.vida==0.0){
            this.status=status.MORTO;
        }
    }
    
    public String imprimirResultado(){
        return "Dwarf";
    }
}
