import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest{
    
    private final double DELTA = 1e-9;
    
    @Test
    public void elfoDaLuzAtacaComEspadaEPerde21noPrimeiroEGanha10noSegundo(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        feanor.atacarComEspada(new Dwarf("Gimli"));
        assertEquals(79.0,feanor.getVida(), DELTA);
        feanor.atacarComEspada(new Dwarf("Gimli"));
        assertEquals(89.0,feanor.getVida(), DELTA);
        assertEquals(2,feanor.getExperiencia());
    }
    
    @Test
    public void elfoDaLuzAtacaEPerde21DeVida(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Dwarf gimli = new Dwarf("Gimli");
        for (int i=0; i<2; i++){
            feanor.atacarComEspada(gimli);
        }
        assertEquals(89.0,feanor.getVida(),DELTA);
    }
    
    @Test
    public void elfoDaLuzAtaca2VezesEPerde21DeVidaDepoisGanha10(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        feanor.atacarComEspada(new Dwarf("Gimli"));
        feanor.atacarComEspada(new Dwarf("Gimli"));
        assertEquals(89.0,feanor.getVida(),DELTA);
    }

    @Test     
    public void nasceComEspada(){
        ElfoDaLuz elfoLuz = new ElfoDaLuz("Raito");
        assertNotNull(elfoLuz.getInventario().buscar("Espada de Galvorn"));
    }

    
    @Test 
    public void naoPodePerderEspada(){      
        ElfoDaLuz elfoLuz = new ElfoDaLuz("Raito");
        elfoLuz.perderItem(elfoLuz.getInventario().buscar("Flecha"));
        elfoLuz.perderItem(elfoLuz.getInventario().buscar("Espada de Galvorn"));
        assertNotNull(elfoLuz.getInventario().buscar("Espada de Galvorn"));
        assertNull(elfoLuz.getInventario().buscar("Flecha"));
    }

    @Test
    public void perdeVidaAtaqueImparGanhaNoPar(){
        Dwarf dwarf = new Dwarf("Gimli");   
        ElfoDaLuz elfoLuz = new ElfoDaLuz("Raito");
        elfoLuz.atacarComEspada(dwarf);
        assertEquals(1, elfoLuz.getExperiencia());
        assertEquals(79.0 , elfoLuz.getVida(), DELTA);
        elfoLuz.atacarComEspada(dwarf);
        assertEquals(2, elfoLuz.getExperiencia());
        assertEquals(89.0 , elfoLuz.getVida(), DELTA);
    }
    
    @Test
    public void elfoDaLuzMorreAtaqueImparVidaInsuficiente(){
        Dwarf dwarf = new Dwarf("Gimli");   
        ElfoDaLuz elfoLuz = new ElfoDaLuz("Raito");
        elfoLuz.atacarComEspada(dwarf);
        assertEquals(1, elfoLuz.getExperiencia());
        assertEquals(0.0 , elfoLuz.getVida(), DELTA);
        assertEquals(Status.MORTO, elfoLuz.getStatus());
    }
    
    @Test
    public void elfoDaLuzNaoAtacaQuandoMorto(){
        Dwarf dwarf = new Dwarf("Gimli");   
        ElfoDaLuz elfoLuz = new ElfoDaLuz("Raito");
        elfoLuz.atacarComEspada(dwarf);
        assertEquals(0, elfoLuz.getExperiencia());
        assertEquals(0.0 , elfoLuz.getVida(), DELTA);
        assertEquals(Status.MORTO, elfoLuz.getStatus());
    }
}
