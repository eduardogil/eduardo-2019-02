package br.com.dbccompany.bancodigital;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Dto.PaisesDTO;
import br.com.dbccompany.bancodigital.Entity.Enderecos;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Paises;
import br.com.dbccompany.bancodigital.Service.AgenciasService;
import br.com.dbccompany.bancodigital.Service.BairrosService;
import br.com.dbccompany.bancodigital.Service.BancosService;
import br.com.dbccompany.bancodigital.Service.CidadesService;
import br.com.dbccompany.bancodigital.Service.ClientesService;
import br.com.dbccompany.bancodigital.Service.EnderecosService;
import br.com.dbccompany.bancodigital.Service.EstadosService;
import br.com.dbccompany.bancodigital.Service.PaisesService;

public class Main {

	private static final Logger LOG = Logger.getLogger(Main.class.getName());
	
	public static void main(String[] args) {
		PaisesService service = new PaisesService();
		PaisesDTO paises = new PaisesDTO();
		paises.setNome("Brasil");
		service.salvarPaises(paises);
		PaisesService service1 = new PaisesService();
		PaisesDTO paises1 = new PaisesDTO();
		paises1.setNome("Argentina");
		service1.salvarPaises(paises1);
		PaisesService service2 = new PaisesService();
		PaisesDTO paises2 = new PaisesDTO();
		paises2.setNome("Chile");
		service2.salvarPaises(paises2);
		
		EstadosService estadoService = new EstadosService();
		EstadosDTO estados = new EstadosDTO();		
		estados.setNome("Rio Grande do Sul");
		estados.setPaises(paises);
		estadoService.salvarEstados(estados);
		EstadosService estadoService1 = new EstadosService();
		EstadosDTO estados1 = new EstadosDTO();		
		estados1.setNome("Santa Catarina");
		estados1.setPaises(paises);
		estadoService1.salvarEstados(estados1);
		EstadosService estadoService2 = new EstadosService();
		EstadosDTO estados2 = new EstadosDTO();		
		estados2.setNome("Sao Paulo");
		estados2.setPaises(paises);
		estadoService2.salvarEstados(estados2);
		EstadosService estadoService3 = new EstadosService();
		EstadosDTO estados3 = new EstadosDTO();		
		estados3.setNome("Rio de Janeiro");
		estados3.setPaises(paises);
		estadoService3.salvarEstados(estados3);
		
		CidadesService cidadeService = new CidadesService();
		CidadesDTO cidades = new CidadesDTO();
		cidades.setNome("Gravatai");
		cidades.setEstados(estados);
		cidadeService.salvarCidades(cidades);

		BairrosService bairroService = new BairrosService();
		BairrosDTO bairros = new BairrosDTO();
		bairros.setCidades(cidades);
		bairros.setNome("Sao Vicente");
		bairroService.salvarBairros(bairros);
		
		EnderecosService enderecoService = new EnderecosService();
		EnderecosDTO enderecos = new EnderecosDTO();
		enderecos.setBairros(bairros);
		enderecos.setNome("Americo Vespucio");
		enderecos.setNumero(242);
		enderecos.setComplemento("Casa1");
		enderecoService.salvarEnderecos(enderecos);
		
		BancosService bancoService = new BancosService();
		BancosDTO bancos = new BancosDTO();
		bancos.setNome("Bradesco");
		bancos.setCodigo(10);
		bancoService.salvarBancos(bancos);
		BancosService bancoService1 = new BancosService();
		BancosDTO bancos1 = new BancosDTO();
		bancos1.setNome("Itau");
		bancos1.setCodigo(15);
		bancoService1.salvarBancos(bancos1);
		BancosService bancoService2 = new BancosService();
		BancosDTO bancos2 = new BancosDTO();
		bancos2.setNome("Banrisul");
		bancos2.setCodigo(20);
		bancoService2.salvarBancos(bancos2);
		
		AgenciasService agenciaService = new AgenciasService();
		AgenciasDTO agencias = new AgenciasDTO();
		agencias.setBancos(bancos);
		agencias.setNome("DBC Bank");
		agencias.setCodigo(999);
		agenciaService.salvarAgencias(agencias);
		AgenciasService agenciaService1 = new AgenciasService();
		AgenciasDTO agencias1 = new AgenciasDTO();
		agencias1.setBancos(bancos1);
		agencias1.setNome("WEB Bank");
		agencias.setCodigo(666);
		agenciaService1.salvarAgencias(agencias1);
		AgenciasService agenciaService2 = new AgenciasService();
		AgenciasDTO agencias2 = new AgenciasDTO();
		agencias2.setBancos(bancos2);
		agencias2.setNome("ROBAUTO Bank");
		agencias.setCodigo(111);
		agenciaService2.salvarAgencias(agencias2);
		
		
		ClientesService clienteService = new ClientesService();
		ClientesDTO clientes = new ClientesDTO();
		clientes.setNome("Zeca");
		clientes.setCpf("02412598718");
		clientes.setRg("3152021549");
		clientes.setConjuge("Maria");
		clientes.setDataNascimento("17111900");
		clientes.setEnderecos(enderecos);
		clienteService.salvarClientes(clientes);
		
		System.exit(0);
	}
	
//	public static void main(String[] args) {
//		Session session = null;
//		Transaction transaction = null;
//		try {
//			session = HibernateUtil.getSession();
//			transaction = session.beginTransaction();
//			
//			Paises paises = new Paises();
//			paises.setNome("Brasil");
//			
//			session.save(paises);
//			
//			/*//session.createQuery("select * from paises").executeUpdate();   // Forma HQL!*/
//			
//			Criteria criteria = session.createCriteria(Paises.class);
//			criteria.createAlias("nome", "nome_paises");
//			criteria.add(
//					Restrictions.isNotNull("nome")
//			);
//			
//			List<Paises> listPaises = criteria.list();
//			
//			transaction.commit();
//		} catch(Exception e){
//			if (transaction != null) {
//				transaction.rollback();
//			}
//			LOG.log(Level.SEVERE, e.getMessage(), e);
//			System.exit(1);
//		} finally {
//			if (session != null){
//				session.close();
//			}
//		}
//		System.exit(0);
//	}

}
