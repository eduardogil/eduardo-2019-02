import React, {Component} from 'react';
import * as axios from 'axios';

export default class Elfos extends Component{
    constructor ( props ) {
        super( props )
        this.state = {
            ListaElfos:[],
            Elfos:[] 
        }
    }
    componentDidMount() {
        const header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:8080/api/elfo/', header)
        .then(resp=>
            {   console.log(resp)
                const elfo = resp.data;
                this.setState({
                    ListaElfos : elfo/* ,
                    Elfos : elfo */
                })
            })
    }
    render(){
        console.log("antes do const")
        const { ListaElfos } = this.state
        return(
            <React.Fragment>   
                { ListaElfos ? ListaElfos.map( elfo => {
                    return(
                        <React.Fragment>
                        <div clasName="container">    
                            <a className="col box">
                            <h4>ID: {elfo.id}</h4>
                            <h4>Nome: {elfo.nome}</h4>
                            <h4>Dano: {elfo.dano}</h4>
                            <h4>Vida: {elfo.vida}</h4>
                            <h4>Experiência: {elfo.experiencia}</h4>
                            </a>
                        </div>
                        </React.Fragment>
                    )
                })
                :
                <div>
                    <h1>Não possui Elfo Cadastrado!</h1>
                </div>
            }
            
            </React.Fragment>
        )
    }
}