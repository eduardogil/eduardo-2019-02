package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Contatos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContatosRepository extends CrudRepository<Contatos, Integer > {
    List<Contatos> findAll();
    Contatos findByValor(String valor);
}
