package br.com.dbccompany.vemserSpring.Repository;


import static org.assertj.core.api.Assertions.assertThat;
import br.com.dbccompany.vemserSpring.Entity.Elfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ElfoRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ElfoRepository elfoRepository;

    @Test
    public void procurarElfoPorNome(){

        Elfo elfo = new Elfo();
        elfo.setNome("Legolas");
        entityManager.persist(elfo);
        entityManager.flush();

        Elfo resposta = elfoRepository.findByNome(elfo.getNome());

        assertThat(resposta.getNome()).isEqualTo(elfo.getNome());
    }
}
