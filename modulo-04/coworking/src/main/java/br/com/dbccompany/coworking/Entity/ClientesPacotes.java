package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ClientesPacotes extends AbstractEntity{
    @Id
    @Column(name = "ID_CLIENTES_PACOTES")
    @SequenceGenerator(allocationSize = 1, name = "CLIENTES_PACOTES_SEQ", sequenceName = "CLIENTES_PACOTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "QUANTIDADE")
    private Integer quantidade;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_CLIENTE")
    private Clientes clientes;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_PACOTE")
    private Pacotes pacotes;

    @OneToMany(mappedBy = "clientesPacotes", cascade = CascadeType.MERGE)
    private List<Pagamentos> pagamentos = new ArrayList<>();

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public Clientes getCliente() {
        return clientes;
    }

    public void setCliente(Clientes clientes) {
        this.clientes = clientes;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}