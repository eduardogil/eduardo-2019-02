import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest{
    
    private final double DELTA = 1e-9;
         
    @Test
    public void elfoNoturnoNasceCom100DeVida(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Gimli");
        assertEquals(100.00, novoElfoVerde.getVida(), DELTA);
    }
    
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXP(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Gimli");
        Dwarf novoAnao = new Dwarf("Mulungrid");
        novoElfoVerde.atacarComFlecha(novoAnao);
        assertEquals(2,novoElfoVerde.getExperiencia());
        assertEquals(1, novoElfoVerde.getQtdFlecha());
        assertEquals(100.00,novoAnao.getVida(), DELTA);
    }
    
    @Test
    public void atirarFlecha3VezesDevePerderFlechaAumentarXp(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Gimli");
        Dwarf novoAnao = new Dwarf("Mulungrid");
        novoElfoVerde.atacarComFlecha(novoAnao);
        novoElfoVerde.atacarComFlecha(novoAnao);
        novoElfoVerde.atacarComFlecha(novoAnao);
        assertEquals(4,novoElfoVerde.getExperiencia());
        assertEquals(0, novoElfoVerde.getQtdFlecha());
    }
    
    @Test
    public void elfosNascemCom4Flechas(){
       ElfoVerde novoElfoVerde = new ElfoVerde("Gimli");
        assertEquals(2, novoElfoVerde.getQtdFlecha());
    }
    
    @Test
    public void elfosNascemComStatusRecemCriado(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Gimli");
        assertEquals(Status.RECEM_CRIADO, novoElfoVerde.getStatus());
    }
    
    
    @Test
    public void elfoVerdeGanha2XpPorUmaFlecha(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Gimli");
        novoElfoVerde.atacarComFlecha(new Dwarf("Balin"));
        assertEquals(2,novoElfoVerde.getExperiencia());
    }
    
    @Test
    public void elfoVerdeAdicionaItemComDescricaoValida(){
         ElfoVerde novoElfoVerde = new ElfoVerde("Gimli");
         Item arcoDeVidro = new Item (1,"Arco de Vidro");
         novoElfoVerde.ganharItem(arcoDeVidro);
         Inventario inventario = novoElfoVerde.getInventario();
         assertEquals(new Item (1,"Arco"),inventario.obter(0));
         assertEquals(new Item (2,"Flecha"),inventario.obter(1));
         assertEquals(arcoDeVidro, inventario.obter(2));
    
    }
    
    @Test
    public void elfoVerdeAdicionaItemComDescricaoInvalida(){
         ElfoVerde novoElfoVerde = new ElfoVerde("Gimli");
         Item arcoDeMadeira = new Item (1,"Arco de Madeira");
         novoElfoVerde.ganharItem(arcoDeMadeira);
         Inventario inventario = novoElfoVerde.getInventario();
         assertEquals(new Item (1,"Arco"),inventario.obter(0));
         assertEquals(new Item (2,"Flecha"),inventario.obter(1));
         assertNull(inventario.buscar("Arco de Madeira"));
    
    }
    
    @Test
    public void elfoVerdePerdeItemComDescricaoValida(){
         ElfoVerde novoElfoVerde = new ElfoVerde("Gimli");
         Item arcoDeVidro = new Item (1,"Arco de Vidro");
         novoElfoVerde.ganharItem(arcoDeVidro);
         novoElfoVerde.perderItem(arcoDeVidro);
         Inventario inventario = novoElfoVerde.getInventario();
         assertEquals(new Item (1,"Arco"),inventario.obter(0));
         assertEquals(new Item (2,"Flecha"),inventario.obter(1));
         assertNull(inventario.buscar("Arco de Vidro"));
    }
    
    @Test
    public void elfoVerdePerdeItemComDescricaoInvalida(){
         ElfoVerde novoElfoVerde = new ElfoVerde("Gimli");
         Item arco = new Item (1,"Arco");
         novoElfoVerde.perderItem(arco);
         Inventario inventario = novoElfoVerde.getInventario();
         assertEquals(new Item (1,"Arco"),inventario.obter(0));
         assertEquals(new Item (2,"Flecha"),inventario.obter(1));
    }
    
}
