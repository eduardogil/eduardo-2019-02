import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest{
    
    private final double DELTA = 1e-9;
    
    @Test
    public void elfoNoturnoNasceCom100DeVida(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("Gimli");
        assertEquals(100.00, novoElfoNoturno.getVida(), DELTA);
    }
    
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXP(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("Gimli");
        Dwarf novoAnao = new Dwarf("Mulungrid");
        novoElfoNoturno.atacarComFlecha(novoAnao);
        assertEquals(3,novoElfoNoturno.getExperiencia());
        assertEquals(1, novoElfoNoturno.getQtdFlecha());
        assertEquals(100.00,novoAnao.getVida(), DELTA);
    }
    
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXPPerderVida(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("Gimli");
        Dwarf novoAnao = new Dwarf("Mulungrid");
        novoElfoNoturno.atacarComFlecha(novoAnao);
        assertEquals(3,novoElfoNoturno.getExperiencia());
        assertEquals(1, novoElfoNoturno.getQtdFlecha());
        assertEquals(100.00,novoAnao.getVida(), DELTA);
        assertEquals(85.00,novoElfoNoturno.getVida(),DELTA);
    }
    
    @Test
    public void atirarFlecha3VezesDevePerderFlechaAumentarXp(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("Gimli");
        Dwarf novoAnao = new Dwarf("Mulungrid");
        novoElfoNoturno.atacarComFlecha(novoAnao);
        novoElfoNoturno.atacarComFlecha(novoAnao);
        novoElfoNoturno.atacarComFlecha(novoAnao);
        assertEquals(6,novoElfoNoturno.getExperiencia());
        assertEquals(0, novoElfoNoturno.getQtdFlecha());
    }
    
    @Test
    public void elfosNascemCom2Flechas(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("Gimli");
        assertEquals(2, novoElfoNoturno.getQtdFlecha());
    }
        
    @Test
    public void elfosNascemComStatusRecemCriado(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("Gimli");
        assertEquals(Status.RECEM_CRIADO, novoElfoNoturno.getStatus());
    }
    
    @Test
    public void atirarFlecha3VezesDevePerderFlechaAumentarXpPerder30DeVida(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("Gimli");
        Dwarf novoAnao = new Dwarf("Mulungrid");
        novoElfoNoturno.atacarComFlecha(novoAnao);
        novoElfoNoturno.atacarComFlecha(novoAnao);
        novoElfoNoturno.atacarComFlecha(novoAnao);
        assertEquals(6,novoElfoNoturno.getExperiencia());
        assertEquals(0, novoElfoNoturno.getQtdFlecha());
        assertEquals(70.00,novoElfoNoturno.getVida(),DELTA);
    }
    
    @Test
    public void elfoVerdeGanha3XpPorUmaFlecha(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("Gimli");
        novoElfoNoturno.atacarComFlecha(new Dwarf("Balin"));
        assertEquals(3,novoElfoNoturno.getExperiencia());
    }
    
    @Test
    public void elfoNoturnoGanha3XpAoAtirarFlecha(){
        Elfo elfoNoturno = new ElfoNoturno("Noturno");
        elfoNoturno.atacarComFlecha(new Dwarf("Gimli"));
        assertEquals(3,elfoNoturno.getExperiencia());
    }
    
    @Test
    public void elfoNoturnoAtiraFlechaEPerde15(){
        Elfo elfoNoturno = new ElfoNoturno("Noturno");
        elfoNoturno.atacarComFlecha(new Dwarf("Gimli"));
        assertEquals(85.0,elfoNoturno.getVida(), DELTA);
        assertEquals(Status.SOFREU_DANO, elfoNoturno.getStatus());
    }
    
    @Test
    public void elfoNoturnoAtira7FlechaEMorre(){
        Elfo elfoNoturno = new ElfoNoturno("Noturno");
        elfoNoturno.getFlecha().setQuantidade(1000);
        elfoNoturno.atacarComFlecha(new Dwarf("Gimli"));
        elfoNoturno.atacarComFlecha(new Dwarf("Gimli"));
        elfoNoturno.atacarComFlecha(new Dwarf("Gimli"));
        elfoNoturno.atacarComFlecha(new Dwarf("Gimli"));
        elfoNoturno.atacarComFlecha(new Dwarf("Gimli"));
        elfoNoturno.atacarComFlecha(new Dwarf("Gimli"));
        elfoNoturno.atacarComFlecha(new Dwarf("Gimli"));
        assertEquals(0.0,elfoNoturno.getVida(), DELTA);
        assertEquals(Status.MORTO, elfoNoturno.getStatus());
    }
}
