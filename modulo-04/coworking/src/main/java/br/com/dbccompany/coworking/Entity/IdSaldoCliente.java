package br.com.dbccompany.coworking.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class IdSaldoCliente implements Serializable {
    public static final long serialVersionUID = 1L;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_cliente", insertable = false, updatable = false)
    private Clientes clientes;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_espaco", insertable = false, updatable = false)
    private Espacos espacos;

    public IdSaldoCliente(Clientes clientes, Espacos espacos){
        this.clientes = clientes;
        this.espacos = espacos;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }
}
