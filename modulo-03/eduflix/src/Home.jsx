import React, { Component } from 'react';
import './css/App.css';
import './css/cabecalho.css';
import './css/button.css';
import './img/Logo.jpg';
import { Link } from 'react-router-dom';

export default class Home extends Component {

render() {/* 
  const{ listaSeries } = this.state */
  return (
    <div className="App">
    <header className="header">
      <a>DBC PLAY</a>
    </header>
    <div className="container">
      <div className="col-lg-50">
        <Link className="link" to="/InjoJsFlix">DBC Play!</Link>
      </div>
      <div className="col-lg-50">
        <Link className="link" to="/BlackMirror">Black Mirror!</Link>
      </div>
    </div>
    </div>
    );
  }
}