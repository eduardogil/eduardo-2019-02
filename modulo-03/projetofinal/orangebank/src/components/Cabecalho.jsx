
import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Link } from 'react-router-dom';

export default class Cabecalho extends Component{
    constructor(props){
        super(props)
        this.history = props.history
    }

    logout(){
        localStorage.removeItem('Authorization')
    }
    
    render(){
        return(
            <header>
                <div className="cabecalho">
                <ul>
                    <li>
                        <Link className="link" to="/agencias">Agencias</Link>
                    </li>
                    <li>
                        <Link className="link" to='/clientes'>Clientes</Link>
                    </li>
                    <li>
                        <Link className="link" to='/tipos'>Tipos de Contas</Link>
                    </li>
                    <li>
                        <Link className="link" to='/contaClientes'>Contas de Clientes</Link>
                    </li>
                    <button className="deslogar" onClick={this.logout.bind(this)}>Deslogar</button>
                </ul>
                </div>
            </header>
        )
    }
}