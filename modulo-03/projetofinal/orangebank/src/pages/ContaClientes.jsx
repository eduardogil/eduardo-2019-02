import React, {Component} from 'react';
import BuscarApi from '../components/BuscarApi';
import Buscar from '../components/Buscar';
import "../styles/ContaClientes.css";

export default class ContaClientes extends Component{
    constructor(props){
        super(props)
        this.api = new BuscarApi()
        this.state = {
            ContasTodosClientes : this.api.buscarTodasContasDeClientes
        }
    }
    componentWillMount(){
        this.api.buscarContasDeClientes()
         setTimeout(() => {
            this.setState({
                ContasTodosClientes: this.api.buscarTodasContasDeClientes
            })
        }, 1000);
    }

    render(){
        const {ContasTodosClientes} = this.state
        return(
            <React.Fragment>    
            <div>
                {ContasTodosClientes ? ContasTodosClientes.map( ContasXCliente => {
                    return(
                        <React.Fragment>
                        <div className="container">
                            <a className="minibox">
                                <h4>Cliente: {ContasXCliente.cliente.nome}</h4>
                                <h5> Tipo: {ContasXCliente.tipo.nome}</h5>
                                <h6> ID:{ContasXCliente.id}</h6>
                            </a>
                        </div>
                        </React.Fragment>
                    )
                })
                :
                <div>
                    <h1></h1>
                </div>
            }
            </div>
            </React.Fragment>
        )
    }
}