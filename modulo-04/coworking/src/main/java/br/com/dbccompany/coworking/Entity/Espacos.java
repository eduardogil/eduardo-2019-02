package br.com.dbccompany.coworking.Entity;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "ESPACOS")
public class Espacos extends AbstractEntity{
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ")
    @GeneratedValue(generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ESPACOS")
    private Integer id;

    @Column(name = "NOME", length = 100, nullable = false, unique = true)
    private String nome;

    @Column(name = "QTDPESSOAS", length = 100 , nullable = false)
    private Integer qtdPessoas;

    @Column(name = "VALOR", length = 100, nullable = false)
    private Integer valor;

    @OneToMany(mappedBy = "espacos", cascade = CascadeType.MERGE)
    private List<Contratacao> contratacao = new ArrayList<>();

    @OneToMany(mappedBy = "espacos", cascade = CascadeType.MERGE)
    private List<SaldoCliente> saldoCliente = new ArrayList<>();

    @OneToMany(mappedBy = "espacos", cascade = CascadeType.MERGE)
    private List<EspacosPacotes> espacosPacotes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Contratacao> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<Contratacao> contratacao) {
        this.contratacao = contratacao;
    }

    public List<SaldoCliente> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<SaldoCliente> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public List<EspacosPacotes> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotes> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }
}
