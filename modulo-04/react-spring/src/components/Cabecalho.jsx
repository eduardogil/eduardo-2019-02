
import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Link } from 'react-router-dom';

export default class Cabecalho extends Component{
    constructor(props){
        super(props)
        this.history = props.history
    }

    logout(){
        localStorage.removeItem('Authorization')
        /* this.props.history.push("/") */
        console.log("Deslogado!")
    }
    
    render(){
        return(
            <header>
                <div className="cabecalho">
                <ul>
                    <li>
                        <Link className="link" to="/dwarfs">Dwarfs</Link>
                    </li>
                    <li>
                        <Link className="link" to='/elfos'>Elfos</Link>
                    </li>
                    <li>
                        <Link className="link" to='/Adicionar'>Adicionar</Link>
                    </li>
                    
                    <button className="deslogar" onClick={this.logout.bind(this)}>Deslogar</button>
                </ul>
                </div>
            </header>
        )
    }
}