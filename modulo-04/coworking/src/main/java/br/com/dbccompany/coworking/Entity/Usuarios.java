package br.com.dbccompany.coworking.Entity;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.time.DurationMin;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "Usuarios")
public class Usuarios extends AbstractEntity{

    @Id
    @SequenceGenerator(allocationSize = 1, name = "USUARIOS_SEQ", sequenceName = "USUARIOS_SEQ")
    @GeneratedValue(generator = "USUARIOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_USUARIO")
    private Integer id;

    @Column(name = "NOME")
    private String nome;

    @NotNull
    @Column(name = "EMAIL", unique = true)
    private String email;

    @NotNull
    @Column(name = "LOGIN", unique = true)
    private String login;

    @Length(min = 6, message = "Senha deve possuir no mínimo 6 Caracteres.")
    @Column(name = "SENHA")
    private String senha;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}