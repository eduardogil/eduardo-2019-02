package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Enum.TipoContratacao;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class SaldoCliente {

    @EmbeddedId
    private IdSaldoCliente id;

    @Column(name = "QUANTIDADE", nullable = false)
    private Integer quantidade;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    @Column(name = "VENCIMENTO", nullable = false)
    private Date vencimento;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_ESPACOS")
    private Espacos espacos;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_CLIENTES")
    private Clientes clientes;

    @OneToMany(mappedBy = "saldoCliente", cascade = CascadeType.MERGE)
    private List<Acessos> acessos = new ArrayList<>();

    public IdSaldoCliente getId() {
        return id;
    }

    public void setId(IdSaldoCliente id) {
        this.id = id;
    }

    public Espacos getEspaco() {
        return espacos;
    }

    public void setEspaco(Espacos espaco) {
        this.espacos = espaco;
    }

    public List<Acessos> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<Acessos> acessos) {
        this.acessos = acessos;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }
}