import React, { Component } from 'react';
import * as axios from 'axios';
import './css/App.css';
import ListaEpisodios from './models/ListaEpisodios';
import EpisodioPadrao from './components/EpisodioPadrao'
import TesteRenderizacao from './components/TesteRenderizacao';
import { Link } from 'react-router-dom';
import "./css/footer.css"
import MensagemFlash from './components/MensagemFlash';
import MensagemFlashExemplo from './components/MensagemFlashExemplo';
import "./css/mirror.css";
import MensagemFlashConst from '../components/MensagemFlashConst';
import MeuInputNumeroExemplo from '../components/MeuInputNumeroExemplo';


class BlackMirror extends Component {
  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    this.sortear = this.sortear.bind( this )
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false,
      mensagem: '',
      deveExibirErro: false
    }
  }

  componentDidMount(){
    axios.get('https://pokeapi.co/api/v2/pokemon/').then( response => console.log(response) )
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState({
      episodio
    })
  }

  alterarEstado(){
    this.setState( {
      exibirMensagem: false
      })
  }

  registrarNota( { nota, erro }){
    this.setState({
      deveExibirErro: erro
    })
    if( erro ){
      return;
    }

    const { episodio } = this.state
    let cor, mensagem
    if( episodio.validarNota( nota )){
      episodio.avaliar( nota )
      cor = 'verde'
      mensagem = MensagemFlashConst.SUCESSO.REGISTRAR_NOTA
    }else{
       cor = 'vermelho'
       mensagem = MensagemFlashConst.ERRO.NOTA_INVALIDA
      }
    this.exibirMensagem({ cor, mensagem })
  }

  exibirMensagem = ({cor, mensagem}) => {
    this.setState({
      cor,
      mensagem,
      exibirMensagem: true
    })
  }

  atualizarMensagem = devoExibir =>{
    this.setState({
      exibirMensagem: devoExibir
    })
  }

  render() {
    const{ episodio, exibirMensagem, notaValida, cor, mensagem} = this.state

    return (
      <div className="App backMirror">
        
        <div className="header">
          <a>Black Mirror</a>
          <MensagemFlashExemplo atualizarMensagem = { this.atualizarMensagem} 
                                
                                animacao = { true } 
                                deveExibirMensagem = { exibirMensagem } 
                                cor = { cor } segundos = { 5 }
                                mensagem = {mensagem}>
          </MensagemFlashExemplo>
        </div>
        <div className="card">
          <EpisodioPadrao episodio = { episodio } sortearNoComp = { this.sortear.bind( this ) } marcarNoComp = {this.marcarComoAssistido}></EpisodioPadrao>
          <MeuInputNumeroExemplo placeholder= "1 a 5"
                                  mensagemCampo="Qual sua nota para esse episodio?"
                                  obrigatorio={ true}
                                  atualizarValor={ this.registrarNota}
                                  deveExibirErro={ deveExibirErro}
                                  visivel={ episodio.assistido || false }>

          </MeuInputNumeroExemplo>
          <MensagemFlash exibirMensagem = { exibirMensagem } notaValida = { notaValida } alterarEstado = {this.alterarEstado.bind( this )}/>
        </div>
        <footer>
          <Link className="link" to="/">Home</Link>
        </footer>
      </div>
    );
  }
}

export default BlackMirror;


  