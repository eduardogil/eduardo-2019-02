import React, { Component } from 'react';
import './App.css';
import ListaTime from './models/ListaTime';
/* import Filho from './exemplos/Filho'
/* 
import Familia from './exemplos/Familia' */
// import CompA, { CompB } from './ExemploComponenteBasico';

class App extends Component {
  constructor( props ) {
    super( props )
    this.ListaTime = new ListaTime()
    console.log(this.ListaTime)
  }

  /* exibirTimeEstado() {
    this.ListaTime.todos.map((time) => {
      return `Time: ${time.nome} | Estado: ${time.estado} `
    }) 
} */

  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/* <Filho titulo={ 'Nome' }/> */}
          <Familia nome= {'Antonio'} sobrenome={'Pereira'}/>
          <Familia nome= {'Pedro'} sobrenome={'Pereira'}/>
          <Familia nome= {'Maria'} sobrenome={'Pereira'}/>
          <Familia nome= {'Carlos'} sobrenome={'Pereira'}/>
          <ul>
            { this.ListaTime.todos.map((time) => {
              return `Time: ${time.nome} | Estado: ${time.estado} `
                          
            }) }
          </ul>
        </header>
      </div>
    );
  }
}


export default App;
