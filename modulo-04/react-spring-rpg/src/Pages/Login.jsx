import React, { Component } from 'react';
import * as axios from 'axios';

import '../styles/header.css';
import '../styles/login.css';

export default class Login extends Component {
    constructor(props){
        super(props)
        this.state = {
            email: '',
            senha: ''
        }
        this.trocaValoresState = this.trocaValoresState.bind( this )
    }

    trocaValoresState(e){
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }
    
    logar(e){
        e.preventDefault();

        const { email, senha } = this.state
        if (email && senha){
            axios.post('http://localhost:1337/login',{
                email: this.state.email,
                senha: this.state.senha
            }).then( resp =>{
                localStorage.setItem( 'Authorization', resp.data.token );
                this.props.history.push("/agencias")
                }
            )
        }
    }


    render(){
        return (
            <React.Fragment>
                <div className="display">
                    <h2>LOGAR</h2>
                    <input type="text" name="email" id="email" placeholder="Digite seu email" onChange={ this.trocaValoresState }/>
                    <input type="senha" name="senha" id="senha" placeholder="Digite sua senha" onChange={ this.trocaValoresState }/>
                    <button id="login" onClick={ this.logar.bind( this ) }>Logar</button>

                </div>
            </React.Fragment>
        )
    }

}