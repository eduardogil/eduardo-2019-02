import PropTypes from 'prop-types';
import Serie from "./Serie";



export default class ListaSeries {
    constructor() {
        this.series = [
            {titulo:"Stranger Things", anoEstreia:2016, diretor:["Matt Duffer","Ross Duffer"], genero:["Suspense","Ficcao Cientifica","Drama"], elenco:["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"], temporadas:2, numeroEpisodios:17, distribuidora:"Netflix"},
            {titulo:"Game Of Thrones", anoEstreia:2011, diretor:["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"], genero:["Fantasia","Drama"], elenco:["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"], temporadas:7, numeroEpisodios:67, distribuidora:"HBO"},
            {titulo:"The Walking Dead", anoEstreia:2010, diretor:["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"], genero:["Terror","Suspense","Apocalipse Zumbi"], elenco:["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"], temporadas:9, numeroEpisodios:122, distribuidora:"AMC"},
            {titulo:"Band of Brothers", anoEstreia:20001, diretor:["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"], genero:["Guerra"], elenco:["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"], temporadas:1, numeroEpisodios:10, distribuidora:"HBO"},
            {titulo:"The JS Mirror", anoEstreia:2017, diretor:["Lisandro","Jaime","Edgar"], genero:["Terror","Caos","JavaScript"], elenco:["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"], temporadas:1, numeroEpisodios:40, distribuidora:"DBC"},
            {titulo:"10 Days Why", anoEstreia:2010, diretor:["Brendan Eich"], genero:["Caos","JavaScript"], elenco:["Brendan Eich","Bernardo Bosak"], temporadas:10, numeroEpisodios:10, distribuidora:"JS"},
            {titulo:"Mr. Robot", anoEstreia:2018, diretor:["Sam Esmail"], genero:["Drama","Techno Thriller","Psychological Thriller"], elenco:["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"], temporadas:3, numeroEpisodios:32, distribuidora:"USA Network"},
            {titulo:"Narcos", anoEstreia:2015, diretor:["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"], genero:["Documentario","Crime","Drama"], elenco:["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"], temporadas:3, numeroEpisodios:30, distribuidora:null},
            {titulo:"Westworld", anoEstreia:2016, diretor:["Athena Wickham"], genero:["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"], elenco:["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"], temporadas:2, numeroEpisodios:20, distribuidora:"HBO"},
            {titulo:"Breaking Bad", anoEstreia:2008, diretor:["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"], genero:["Acao","Suspense","Drama","Crime","Humor Negro"], elenco:["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"], temporadas:5, numeroEpisodios:62, distribuidora:"AMC"}
        ].map( s => new Serie( s.titulo, s.anoEstreia, s.diretor, s.genero, s.elenco, s.temporadas, s.numeroEpisodios, s.distribuidora))
    }

    seriesInvalidas(){
        const invalidas = []
        const series = new ListaSeries().series
        let now = new Date().getFullYear()
            for( let i = 0; i < series.length; i++ ) {
                let serie = series[ i ]
                const isEmpty = Object.values(series[i]).some(invalida => (invalida == null || invalida === '' || invalida === undefined));
                if(( isEmpty === true ) || ( serie.anoEstreia > now )) {
                    invalidas.push( serie )
                }
            }
        return invalidas
     }

    filtrarPorAno(ano){
        let validasPorAno = []
        validasPorAno = new ListaSeries().series
        return validasPorAno.filter( serie => serie.anoEstreia >= ano )
     }
     
    mediaDeEpisodios() {
        let soma = 0
        let qtdSerie = this.series.length
        this.series.forEach(serie => soma += serie.numeroEpisodios)
        return (soma / qtdSerie)
    }
        
    totalSalarios ( ind ) {
        let diretores = this.series[ind].diretor.length
        let operarios = this.series[ind].elenco.length
        let total = (diretores * 100000.00) + (operarios*40000.00)
        return total
    }

    queroGenero(genero) {
        let listaGenero = this.series.map(serie => serie.genero.includes(genero) ? serie : null)
        listaGenero = listaGenero.filter(serie => serie != null)
        return listaGenero
    }

    queroTitulo(nome) {
        let listaTitulo = this.series.map(serie => serie.titulo.toLowerCase().includes(nome) ? serie : null)
        listaTitulo = listaTitulo.filter(serie => serie != null)
        return listaTitulo
    }

    queroTituloAbr () {
        let palavra = '';
        let elencoSerie = []
        let abr = -1;
        let i = 0;
        let j = 0;
        for( i = 0; i < this.series.length; i++){
            for( j = 0; j < this.series[i].elenco.length; j++){
                if (this.series[i].elenco[j].match(". ") && this.series[i].elenco[j].indexOf(".") !== -1){
                    elencoSerie.push(this.series[i].elenco[j]);
                }
            }
            if(elencoSerie.length === this.series[i].elenco.length){
                for(j = 0; j < elencoSerie.length; j++){
                    abr = elencoSerie[j].indexOf('.' - 1);
                    palavra = elencoSerie[j].subString(abr,abr);
                }
                return palavra;
            }
        }
        return false;
    }
    creditos(ind){
        const titulo = this.series[ind].titulo;
        let diretores = this.series[ind].diretor.map(diretor => diretor.split(' ').reverse().join(' ') )
        diretores.sort()
        diretores = diretores.map(diretor => diretor.split(' ').reverse().join(' ') )
        let elenco = this.series[ind].elenco.map(elenco => elenco.split(' ').reverse().join(' ') )
        elenco.sort()
        elenco = elenco.map(elenco => elenco.split(' ').reverse().join(' ') )
        return { titulo, diretores, elenco }
    }

    hashtag() {
        let resultado;
        this.series.forEach(function (serie) {
            let buscar = serie.elenco.map(e => abr(e))
            if (!buscar.includes(false)) {
                resultado = serie.elenco.map(e => {
                    let ind = e.indexOf('.') - 1
                    return e.substr(ind, 1)
                })
                resultado.unshift('#')
            }
        })
        return resultado.join('')
    }
}
function abr(str) {
    return str.includes('. ')
}

ListaSeries.PropTypes = {
    seriesInvalidas : PropTypes.Array,
    filtrarPorAno : PropTypes.Array,
    mediaEpisodios : PropTypes.Array,
    totalSalarios : PropTypes.Array,
    queroGenero : PropTypes.Array,
    queroTitulo : PropTypes.Array,
    queroTituloAbr : PropTypes.Array,
    abr: PropTypes.array,
    creditos: PropTypes.Array,
    hashtag: PropTypes.array,
}

