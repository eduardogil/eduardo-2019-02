public class Elfo extends Personagem{
    private Atacar atacarComEspada;
    private Atacar atacar;
    private int indiceFlecha;
    protected double qtdDano;
    private static int qtdElfos;
    
    {   
        this.inventario = new Inventario();
        this.indiceFlecha = 1;
    }
    
    public Elfo(String nome){
        super(nome);
        this.vida = 100.00;
        this.inventario.adicionar(new Item(1,"Arco"));
        this.inventario.adicionar(new Item(2,"Flecha"));
        this.qtdDano = 0.0;
        this.qtdExperienciaPorAtaque = 1;
        Elfo.qtdElfos++;
    }
        
    protected void finalize() throws Throwable{
        Elfo.qtdElfos--;
    }
    
    public static int getQtdElfos(){
        return Elfo.qtdElfos;
    }
    
    public Item getFlecha(){
        return this.inventario.obter(indiceFlecha);
    }
        
    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }    
    
    public boolean podeAtirarFlecha(){
        return this.getFlecha().getQuantidade() > 0;
    }
        
    public void atacarComFlecha(Dwarf dwarf){
        int qtdAtual = this.getFlecha().getQuantidade();
        if(podeAtirarFlecha() && podeSofrerDano()){
            this.getFlecha().setQuantidade(qtdAtual - 1);
            this.sofrerDano();
            dwarf.sofrerDano();
            this.aumentarXp();
        }
    }
               
      
    public String imprimirResultado(){
        return "Elfo";
    }
}