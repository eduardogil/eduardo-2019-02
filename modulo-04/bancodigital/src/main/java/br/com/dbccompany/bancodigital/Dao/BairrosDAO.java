package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.Cidades;

public class BairrosDAO extends AbstractDAO<Bairros>  {
	
	private static final CidadesDAO CIDADES_DAO = new CidadesDAO(); 
	
	public Bairros parseFrom(BairrosDTO bairrosdto) {
		Bairros bairros = null;
		if (bairrosdto.getIdBairros() !=null) {
			bairros = buscar(bairrosdto.getIdBairros());
		} else {
			bairros = new Bairros();
		}
		Cidades cidades = CIDADES_DAO.parseFrom( bairrosdto.getCidades() );
		
		bairros.setCidade(cidades);
		bairros.setNome(bairrosdto.getNome());
		return bairros;
	}
	
	@Override
	protected Class<Bairros> getEntityClass() {
		return Bairros.class;
	}
}
