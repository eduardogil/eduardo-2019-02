import React, { Component } from 'react';
import Axios from 'axios';

class ListaAgencias extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listaAgencias:[]
        }
    }

    componentWillMount() {
    Axios.get('http://localhost:1337/agencias', {
        headers: {
            authorization: localStorage.getItem('Authorization')
        }
    }).then(resp => {
        this.setState({
            listaAgencias: resp.data
            })
        })
    }
}

export default ListaAgencias;