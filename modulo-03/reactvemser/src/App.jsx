import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/ListaEpisodios';
import EpisodioPadrao from './components/EpisodioPadrao'
import TesteRenderizacao from './components/TesteRenderizacao'

/* import Filho from './exemplos/Filho'
/* 
import Familia from './exemplos/Familia' */
// import CompA, { CompB } from './ExemploComponenteBasico';

class App extends Component {
  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
   /*  this.sortear = this.sortear.bind( this ) */
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false
    }
  }


  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState({
      episodio
    })
  }

  registrarNota( evt ){
    const { episodio } = this.state
    episodio.avaliar( evt.target.value )
    this.setState({
      episodio,
      exibirMensagem: true
    })
    setTimeout( () =>{
      this.setState( {
        exibirMensagem: false
      })
    }, 5000)
  }

  gerarCampoNota(){
  // https://reactjs.org/docs/conditional-rendering.html
    return(
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Qual sua nota para esse episódio?</span>
              <input type="number" placeholder="1 a 5" onBlur={ this.registrarNota.bind( this )}></input>
            </div>
          )
        }
      </div>
    )
  }


  render() {
    const{ episodio, exibirMensagem } = this.state

    return (
      <div className="App">
        <div className="App-Header">
          {/* EpisodioPadrao episodio = { episodio } sortearNoComp={ this.sortear.bind( this ) } marcarNoComp={this.marcarComoAssistido}/>
            { this.gerarCampoNota()}
          <h5>{ exibirMensagem ? 'Nota registrada com sucesso!' : '' }</h5> */}
          <TesteRenderizacao nome={ 'Eduardo' }>
            <h4>Aqui novamente!</h4>
          </TesteRenderizacao>
        </div>
        
      </div>
    );
  }
}

export default App;
