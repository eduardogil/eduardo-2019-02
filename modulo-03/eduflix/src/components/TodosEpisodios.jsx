import React, { component } from 'react';


/* 
Exercício 05

Crie um componente TodosEpisodios que deverá listar todos episódios, 
independente se foram avaliados ou não. O modo de listagem dos episódios deve ser o mesmo que o utilizado
 no exercício 02. Adicione um botão na PaginalInicial para poder navegar até esta tela. 
 Neste componente TodosEpisodios, você deverá ter dois botões:
Ordenar por data de estréia
Ordenar por duração

Se você clicar uma vez no botão de ordenação, ele deverá exibir os registros do menor para o maior (A-Z),
se você clicar novamente deverá exibir (Z-A)
*/