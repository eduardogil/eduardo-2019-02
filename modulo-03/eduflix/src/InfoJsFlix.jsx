import React, { Component } from 'react';
import './css/App.css';
import ListaSeries from './components/ListaSeries';
import Serie from './components/Serie';
import './css/cabecalho.css';
import './css/button.css';
import './img/Logo.jpg';
import { Link } from 'react-router-dom';
import './css/footer.css';
import SeriePadrao from './components/SeriePadrao';

export default class InfoJsFlix extends Component {
  constructor( props ) {
    super( props )
    this.listaSeries = new ListaSeries()
    this.invalidas = this.listaSeries.seriesInvalidas.bind( this )
    this.seriesAno = this.listaSeries.filtrarPorAno.bind( this )
    this.media = this.listaSeries.mediaDeEpisodios.bind( this )
    this.mascada = this.listaSeries.totalSalarios.bind( this )
    this.genero = this.listaSeries.queroGenero.bind( this )
    this.titulo = this.listaSeries.queroTitulo.bind( this )
    this.creditos = this.listaSeries.creditos.bind( this )
    this.secreta = this.listaSeries.hashtag()
    /* this.state = {isContactModalOpen: false} */

    /* 
    console.log(this.invalidas)
    console.log(this.seriesAno)
    console.log(this.media)
    console.log(this.mascada)
    console.log(this.genero)
    console.log(this.titulo)
    console.log(this.creditos)
    console.log(this.secreta)
     */
  }
  /* 
  invalidas(){
    const invalidas = Array.prototype.invalidas
    this.setState({ series: invalidas })
  }
  titulo( event ) {
    const listaPorTitulo = Array.prototype.queroTitulo( event.target.value )
    this.setState( { series: listaPorTitulo } ) 
  }*/
  renderContactModal () {
    return (
      <div id="modal">
        <form>
          <input type="text" placeholder="Your e-mail" />
          
          <textarea placeholder="What are your thoughts?" />
          
          <button onClick={this.setState({isContactModalOpen: false})}>Close</button>
          <button>Send</button>
        </form>
      </div>
    )
    
  }
  gerarCampoAno(){
    // https://reactjs.org/docs/conditional-rendering.html
      return(
        <div>
          {
            this.seriesAno && (
              <div>
                <span>Digite o ano para filtrar?</span>
                <input type="number" placeholder="Digite o ano para filtrar" onBlur={ this.filtrarPorAno.bind( this )}></input>
              </div>
            )
          }
        </div>
      )
    }

render() { 
  return (
    <div className="App">
    <header className="header">
        <a>DBC PLAY</a>
    </header>
      
    <div className="container">
      <button className="button button-gray" onClick={() => this.invalidas.bind( this )}>Séries Inválidas</button>
      <button className="button button-gray" onClick={() => this.setState({isContactModalOpen: true})}>Selecionar Ano</button>
      <button className="button button-gray" onClick={() => this.titulo()}>Buscar Nome</button>
      <button className="button button-gray" onClick={() => this.mediaDeEpisodios()}>Média de Episódios</button>
      <button className="button button-gray" onClick={() => this.mascada()}>Total Salários</button>
      <button className="button button-gray" onClick={() => this.genero()}>Buscar Gênero</button>
      <button className="button button-gray" onClick={() => this.titulo()}>Buscar Título</button>
      <button className="button button-gray" onClick={() => this.creditos()}>Créditos</button>
      <button className="button button-gray" onClick={() => this.listaSeries.hashtag()}>Hashtag</button>
    </div>
    <footer>
          <Link className="link pull-left" to="/">Home</Link>
    </footer>
    </div>
    );
  }
}
