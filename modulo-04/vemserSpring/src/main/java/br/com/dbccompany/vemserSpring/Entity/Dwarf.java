package br.com.dbccompany.vemserSpring.Entity;

import br.com.dbccompany.vemserSpring.Enum.Status;
import br.com.dbccompany.vemserSpring.Enum.TipoPersonagem;

import javax.persistence.Entity;

@Entity
public class Dwarf extends Personagem {

    public Dwarf()  {
        super.setTipoPersonagem(TipoPersonagem.DWARF);
        super.setStatus(Status.RECEM_CRIADO);
        super.setVida(100.00);
    }
}
