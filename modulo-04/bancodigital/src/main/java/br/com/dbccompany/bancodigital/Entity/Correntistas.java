package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name ="CORRENTISTAS_SEQ", sequenceName = "CORRENTISTAS_SEQ" )
public class Correntistas extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue (generator = "CORRENTISTAS_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_CORRENTISTAS")
	private Integer id;
	
	@Column(name = "razao_social", length = 100, nullable = false)
	private String razaoSocial;
	
	@Column(name = "cnpj", length = 100, nullable = false)
	private String cnpj;
	
	@Column(name = "saldo", length = 100, nullable = false)
	private Double saldo;
	

	public String getCnpj() {
		return cnpj;
	}
	

	@Enumerated(EnumType.STRING)
	private TipoConta tipo;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable( name = "correntistas_x_clientes",
					joinColumns = { @JoinColumn( name = "id_correntistas")},
					inverseJoinColumns = { @JoinColumn( name = "id_clientes" ) })
	private List<Clientes> correntistas_clientes = new ArrayList<>();
	
	
	@ManyToMany (mappedBy = "correntistas")
	private List<Agencias> agencias = new ArrayList<>();
	
	public TipoConta getTipo() {
		return tipo;
	}
	public void setTipo(TipoConta tipo) {
		this.tipo = tipo;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public List<Clientes> getCorrentistas_clientes() {
		return correntistas_clientes;
	}
	public void setCorrentistas_clientes(List<Clientes> correntistas_clientes) {
		this.correntistas_clientes = correntistas_clientes;
	}
	public List<Agencias> getAgencias() {
		return agencias;
	}
	public void setAgencias(List<Agencias> agencias) {
		this.agencias = agencias;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
}	
