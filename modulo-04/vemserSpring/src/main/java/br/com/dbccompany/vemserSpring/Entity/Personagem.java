package br.com.dbccompany.vemserSpring.Entity;

import br.com.dbccompany.vemserSpring.Enum.Status;
import br.com.dbccompany.vemserSpring.Enum.TipoPersonagem;

import javax.persistence.*;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "personagem")
public class Personagem {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ")
    @GeneratedValue (generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_PERSONAGEM")
    private Integer id;

    @Column(name = "nome", length = 100, nullable = false)
    private String nome;

    @Column(name = "vida", length = 100)
    private Double vida;

    @Column(name = "dano", length = 100)
    private Double dano;

    @Column(name = "experiencia", length = 100)
    private Double experiencia;

    /*@Column(name = "inventario", length = 100, nullable = false)
    private Inventario inventario;
*/
    @Enumerated(EnumType.STRING)
    @Column(name = "tipoPersonagem", length = 100)
    private TipoPersonagem tipoPersonagem;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 100)
    private Status status;

    @OneToOne(cascade=CascadeType.ALL)
    private Inventario inventario;

    /*{
        this.status = Status.RECEM_CRIADO;
        this.inventario = new Inventario();
        this.experiencia = 0;
        this.dano= 10.0;
    }*/

    public Double getDano() {
        return dano;
    }

    public void setDano(Double dano) {
        this.dano = dano;
    }

    public TipoPersonagem getTipoPersonagem() {
        return tipoPersonagem;
    }

    public void setTipoPersonagem(TipoPersonagem tipoPersonagem) {
        this.tipoPersonagem = tipoPersonagem;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Double getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Double experiencia) {
        this.experiencia = experiencia;
    }

   /* protected Inventario getInventario() {
        return inventario;
    }

    protected void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }*/
}
