import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoTest
{
    @Test
    public void exercitoNaoceitaElfoLuz(){
        Exercito e = new Exercito();
        ElfoDaLuz elfLuz = new ElfoDaLuz("Raito");
        e.alistar(elfLuz);
        assertEquals(0, e.getExercito().size());
    }

    @Test
    public void exercicitoAceitaVerdeNoturno(){
        Exercito e = new Exercito();
        ElfoVerde ev = new ElfoVerde("Gurin");
        ElfoVerde morto = new ElfoVerde("Verde");
        ElfoNoturno en = new ElfoNoturno("Sylvanas");
        e.alistar(ev);
        e.alistar(morto);
        e.alistar(en); 
        assertEquals(3, e.getExercito().size());
    }

    @Test
    public void buscarElfosPorStatus(){
        Exercito e = new Exercito();
        ElfoVerde ev = new ElfoVerde("Gurin");
        ElfoVerde morto = new ElfoVerde("Verde");
        ElfoNoturno en = new ElfoNoturno("Sylvanas");
        morto.matarPersonagem();
        e.alistar(ev);
        e.alistar(morto);
        e.alistar(en);    
        assertEquals(2, e.getElfosPorStatus(Status.RECEM_CRIADO).size());
    }

    @Test
    public void getFormacaoNoturnosPorUltimo(){
        Exercito e = new Exercito();
        e.alistar(new ElfoVerde("Gurin"));
        e.alistar(new ElfoVerde("Verde"));
        e.alistar(new ElfoNoturno("Sylvanas"));
        e.alistar(new ElfoVerde("Gurin"));
        e.alistar(new ElfoVerde("Verde"));
        e.alistar(new ElfoNoturno("Sylvanas"));
        e.alistar(new ElfoVerde("Gurin"));
        e.alistar(new ElfoVerde("Verde"));
        e.alistar(new ElfoNoturno("Sylvanas"));
        e.alistar(new ElfoVerde("Gurin"));
        e.alistar(new ElfoVerde("Verde"));
        e.alistar(new ElfoNoturno("Sylvanas"));
        e.alistar(new ElfoVerde("Gurin"));
        e.alistar(new ElfoVerde("Verde"));
        e.alistar(new ElfoNoturno("Sylvanas"));
        ElfoVerde morto = new ElfoVerde("Verde");
        ElfoVerde morto2 = new ElfoVerde("Verde");
        e.alistar(morto);
        e.alistar(morto2);
        for(Elfo elfo : e.noturnosPorUltimo()){
            System.out.println(elfo.getClass() + " Status: " + elfo.getStatus());
        }
    }

    @Test
    public void getFormacaoIntercalada(){
        Exercito e = new Exercito();
        e.alistar(new ElfoVerde("Gurin"));
        e.alistar(new ElfoVerde("Verde"));
        e.alistar(new ElfoNoturno("Sylvanas"));
        e.alistar(new ElfoVerde("Gurin"));
        e.alistar(new ElfoVerde("Verde"));
        e.alistar(new ElfoNoturno("Sylvanas"));
        e.alistar(new ElfoVerde("Gurin"));
        e.alistar(new ElfoVerde("Verde"));
        e.alistar(new ElfoNoturno("Sylvanas"));
        e.alistar(new ElfoVerde("Gurin"));
        e.alistar(new ElfoVerde("Verde"));
        e.alistar(new ElfoNoturno("Sylvanas"));
        e.alistar(new ElfoVerde("Gurin"));
        e.alistar(new ElfoVerde("Verde"));
        e.alistar(new ElfoNoturno("Sylvanas"));
        ElfoVerde morto = new ElfoVerde("Verde");
        ElfoVerde morto2 = new ElfoVerde("Verde");
        e.alistar(morto);
        e.alistar(morto2);
        for(Elfo elfo : e.ataqueIntercalado()){
            System.out.println(elfo.getClass() + " Status: " + elfo.getStatus());
        }
    }
}
