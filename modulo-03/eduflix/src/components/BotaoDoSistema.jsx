import React from 'react';
import PropTypes from 'prop-types';

const BotaoDoSistema = ({ cor, texto, quandoClicar }) =>
    
    <React.Fragment>
        <button className = { `button ${ cor } ` } onClick={ quandoClicar }>{ texto }</button>
    </React.Fragment>

BotaoDoSistema.propTypes = {    
    texto: PropTypes.string.isRequired,
    quandoClicar: PropTypes.func.isRequired,
    cor: PropTypes.oneOf( [  'verde', 'azul' ] )
}

BotaoDoSistema.defaultProps = {
    cor: 'verde'
}


export default BotaoDoSistema