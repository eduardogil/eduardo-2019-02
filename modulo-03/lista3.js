function cardapioIFood( veggie = true, comLactose = false ) {
    let cardapio = [
      'enroladinho de salsicha',
      'cuca de uva'
    ]

    if ( comLactose) {
      cardapio.push( 'pastel de queijo' )
    }

    cardapio = [...cardapio,'pastel de carne','empada de legumes marabijosa'];
    
    /* cardapio = cardapio.concat([
      'pastel de carne',
      'empada de legumes marabijosa'
    ]) */
  
    if ( veggie ) {
      // TODO: remover alimentos com carne (é obrigatório usar splice!)
      cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), 1 )
      cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 )
    }
    
    let resultado = cardapio
                        //.filter(alimento => alimento ==='cuca de uva')
                        .map(alimento => alimento.toUpperCase());
            /* 
    console.log(resultado)  */       
    return resultado;
  }
  
  cardapioIFood(true, true) // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]

  function criarSanduiche(pao, recheio, queijo){
    console.log(`Seu Sanduiche tem o pão ${pao} com recheio de ${recheio} e queijo ${queijo}`)

  }
  const ingredientes = ['3 queijos', 'Frango', 'Cheddar'];
  criarSanduiche(...ingredientes);

  function receberValoresIndefinidos(...valores){
    valores.map(valor => console.log(valor));
  }

  receberValoresIndefinidos([1, 3, 4, 5]);

  let teste = {
    nome:"Marcos",
    idade: 29
  }
  /*console.log({..."Marcos"});
    console.log(... "Marcos"); */

  
  let inputTeste = document.getElementById('campoTeste');
  inputTeste.addEventListener('blur',() => cardapioIFood(true, true));
  /* console.log("chegou aqui"); */

/* 
console.log(objetoTeste);  */
  
String.prototype.corre = function(upper = false){
  let texto = `${this} estou Correndo`;
  return upper ? texto.toLocaleUpperCase() : texto;
}

console.log("eu".correr());