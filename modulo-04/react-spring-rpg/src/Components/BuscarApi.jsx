import * as axios from 'axios';

export default class BuscarApi {
    constructor( ){
        this.obj = []
        this.baseUrl = 'http://localhost:1337'
    }

    buscarApi(url,id){
        url += id ? `${id}` : ''
        axios.get( `${this.baseUrl}/${url}`,{
            headers: {
                authorization: localStorage.getItem('Authorization')
            }
        })
        .then( resp => {
            this.obj = resp.data
        }).catch(function (error){
        })
    }

    buscarListaAgencias(){
        this.buscarApi('agencias')
    }
    buscarListaClientes(){
        this.buscarApi('clientes')
    }
    buscarTiposContas(){
        this.buscarApi('tipoContas')
    }
    buscarContasDeClientes(){
        this.buscarApi('conta/clientes')
    }
    buscarAgencia( id ) {
        this.buscarApi( 'agencia/', id )
    }
    buscarContaPorCliente( id ) {
        this.buscarApi( 'conta/cliente/', id )
    }
    buscarPorTipoConta( id ) {
        this.buscarApi( 'tiposConta/', id )
    }
    get buscarDetalhesAgencia() {
        return this.obj.agencias
    }
    get buscarAgencias(){
        return this.obj.agencias
    }
    get buscarClientes(){
        return this.obj.clientes
    }
    get buscarTiposDeContas(){
        return this.obj.tipos
    }
    get buscarTodasContasDeClientes(){
        return this.obj.cliente_x_conta
    }
}