import React, { Component } from 'react';
import Axios from 'axios';

class TipoContas extends Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
    Axios.get('http://localhost:1337/tipoContas', {
        headers: {
            authorization: localStorage.getItem('Authorization')
        }
    }).then(resp => {
        this.setState({
            tipoContas: resp.data.tipos
            })
        })
    }
}

export default TipoContas;