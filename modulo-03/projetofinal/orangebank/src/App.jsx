import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { PrivateRoute } from './components/PrivateRoute';

import Cabecalho from './components/Cabecalho';
import Login from './pages/Login';
import Home from './pages/Home';
import Agencias from './pages/Agencias';
import Clientes from './pages/Clientes';
import Tipos from './pages/Tipos';
import ContaClientes from './pages/ContaClientes';

import './App.css';


export default class App extends Component {
  render(){
    return(
      <div className="App">
        <Router>
          <Cabecalho></Cabecalho>
          <React.Fragment>
            <section>
              <Route path="/login" exact component = { Login } />
              <PrivateRoute path="/Home" exact component = { Home } />
              <PrivateRoute path="/Agencias" exact component = { Agencias } />
              <PrivateRoute path="/Clientes" exact component = { Clientes } />
              <PrivateRoute path="/Tipos" exact component = { Tipos } />
              <PrivateRoute path="/ContaClientes" exact component = { ContaClientes } />
              {/* <PrivateRoute path="/agencia/:id" exact component={  } />
              <PrivateRoute path="/cliente/:id" exact component={  } />
              <PrivateRoute path="/tiposConta/:id" exact component={  } />
              <PrivateRoute path="/conta/cliente/:id" exact component={  } /> */}
            </section>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}