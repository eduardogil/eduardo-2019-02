export default class Serie {
    constructor( titulo, anoEstreia, diretor, genero, elenco, temporadas, numeroEpisodios, distribuidora ){
        this.titulo = titulo
        this.anoEstreia = anoEstreia
        this.diretor = diretor
        this.genero = genero
        this.elenco = elenco
        this.temporadas = temporadas
        this.numeroEpisodios = numeroEpisodios
        this.distribuidora = distribuidora
    }
}
/* 
Serie.propTypes = {
    titulo : PropTypes.string,
    anoEstreia : PropTypes.number,
    diretor : PropTypes.arrayOf(PropTypes.string),
    genero : PropTypes.arrayOf(PropTypes.string),
    elenco : PropTypes.arrayOf(PropTypes.string),
    temporadas : PropTypes.number,
    numeroEpisodios : PropTypes.number,
    distribuidora : PropTypes.string
} */
 