package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Enum.TipoPagamento;

import javax.persistence.*;

@Entity
public class Pagamentos extends AbstractEntity{
    @Id
    @Column(name = "ID_PAGAMENTO")
    @SequenceGenerator(allocationSize = 1, name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
    @GeneratedValue(generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TipoPagamento tipoPagamento;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_CLIENTE_PACOTE")
    private ClientesPacotes clientesPacotes;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_CONTRATACAO")
    private Contratacao contratacao;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesPacotes getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotes clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public ClientesPacotes getClientePacote() {
        return clientesPacotes;
    }

    public void setClientePacote(ClientesPacotes clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}