import React, {Component} from 'react';
import BuscarApi from './../components/BuscarApi';

import "../styles/agencia.css";
import "../styles/tipos.css";

export default class Agencias extends Component{
    constructor ( props ) {
        super( props )
        this.api = new BuscarApi()
        this.state = {
            ListaAgencias : this.api.buscarAgencias
        }
    }
    componentWillMount(){
        this.api.buscarListaAgencias()
         setTimeout(() => {
            this.setState({
                ListaAgencias: this.api.buscarAgencias
            })
        }, 1000);
    }
    render(){
        const { ListaAgencias } = this.state
        return(
            <React.Fragment>   
                { ListaAgencias ? ListaAgencias.map( agencias => {
                    return(
                        <React.Fragment>
                        <div clasName="container">    
                            <a className="col box">
                                <ul>
                                    <h3>Agência: {agencias.nome}</h3>
                                    <a className="information"> 
                                        <h5>ID: {agencias.id}</h5>
                                        <h5>Codigo: {agencias.codigo}</h5>
                                        <h6>Cidade: {agencias.endereco.cidade}</h6> 
                                        <h6>Bairro: {agencias.endereco.bairro}</h6>  
                                        <h6>Logradouro: {agencias.endereco.logradouro} {agencias.endereco.numero}</h6>  
                                        <h6>UF: {agencias.endereco.uf}</h6>
                                    </a> 
                                    <input type="checkbox" /* onClick={() =>this.ehDigital(agencias.id)} */ name="isDigital"></input>
                                    <label for="isDigital">Digital</label>
                                </ul>
                            </a>
                        </div>
                        </React.Fragment>
                    )
                })
                :
                <div>
                    <h1></h1>
                </div>
            }
            
            </React.Fragment>
        )
    }
}