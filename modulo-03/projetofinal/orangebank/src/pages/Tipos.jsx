
import React, {Component} from 'react';
import BuscarApi from './../components/BuscarApi';

import "../styles/tipos.css";

export default class Tipos extends Component{
    constructor(props){
        super(props)
        this.api = new BuscarApi()
        this.state = {
            TipoContas : this.api.buscarTiposDeContas
        }
    }
    componentWillMount(){
        this.api.buscarTiposContas()
         setTimeout(() => {
            this.setState({
                TipoContas: this.api.buscarTiposDeContas
            })
        }, 1000);
    }

    render(){
        const {TipoContas} = this.state
            return(
                <div className="container">
                <React.Fragment>   
                    {TipoContas ? TipoContas.map( tipos => {
                        return(
                            
                            
                            <React.Fragment>
                                    <div className="col minicard">
                                        <h4>{tipos.id}-{tipos.nome}</h4>
                                    </div>
                                
                            </React.Fragment>
                            
                        )
                    })
                    :
                    <div>
                        <h1></h1>
                    </div>
                    }
                </React.Fragment>
                </div>
    )
    }
}
