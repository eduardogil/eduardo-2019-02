package br.com.dbccompany.coworking.Entity;

public abstract class AbstractEntity {
    public abstract Integer getId();
    public abstract void setId(Integer id);
}
