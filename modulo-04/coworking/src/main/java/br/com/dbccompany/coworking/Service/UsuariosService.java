package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Usuarios;
import br.com.dbccompany.coworking.Repository.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public class UsuariosService {

    @Autowired
    private UsuariosRepository usuariosRepository;

    @Transactional( rollbackFor = Exception.class )
    public Usuarios salvar(Usuarios usuarios ) {
        usuarios.setSenha(convertStringToMd5(usuarios.getSenha()));
        return usuariosRepository.save( usuarios );
    }

    @Transactional( rollbackFor = Exception.class )
    public Usuarios editar( Integer id, Usuarios usuarios ) {
        usuarios.setId( id );
        return usuariosRepository.save( usuarios );
    }

    public List<Usuarios> todosUsuarios() {
        return usuariosRepository.findAll();
    }

    private String convertStringToMd5(String valor) {
        MessageDigest mDigest;
        try {
            mDigest = MessageDigest.getInstance("MD5");
            byte[] valorMD5 = mDigest.digest(valor.getBytes("UTF-8"));

            StringBuffer sb = new StringBuffer();
            for (byte b : valorMD5){
                sb.append(Integer.toHexString((b & 0xFF) |
                        0x100).substring(1,3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    //public Usuarios isUsuarioReadyToLogin(String email, String senha) {
    //    try {
    //        email = email.toLowerCase().trim();
    //        logger.info("Verificando login do usuário " + email);
    //        List retorno = dao.findByNamedQuery(
    //                Usuario.FIND_BY_EMAIL_SENHA,
    //                new NamedParams("email", email
    //                        .trim(), "senha", convertStringToMd5(senha)));

    //       if (retorno.size() == 1) {
    //           Usuario userFound = (Usuario) retorno.get(0);
    //           return userFound;
    //       }

    //       return null;
    //   } catch (DAOException e) {
    //       e.printStackTrace();
    //       throw new BOException(e.getMessage());
    //   }
    //}
}
