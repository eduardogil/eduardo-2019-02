public class ElfoNoturno extends Elfo{
    
    public ElfoNoturno(String nome){
        super(nome);
        //this.qtdExperienciaPorAtaque = 3;
        this.qtdDano = 15.0;
        this.qtdExperienciaPorAtaque = 3;
    }
    
    
    @Override
    public void atacarComFlecha(Dwarf dwarf){
        int qtdAtual = this.getFlecha().getQuantidade();
        if(podeAtirarFlecha()){
            this.getFlecha().setQuantidade(qtdAtual - 1);
            dwarf.sofrerDano();
            this.aumentarXp();
            this.vida=this.vida-15;
            this.status=Status.SOFREU_DANO;
            if(this.vida<0){
                this.vida=0.0;
                this.status=Status.MORTO;
            }
        }
    }
}
