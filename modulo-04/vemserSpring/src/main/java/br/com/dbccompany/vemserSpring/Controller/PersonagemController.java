package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.Personagem;
import br.com.dbccompany.vemserSpring.Service.PersonagemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/personagem")
public class PersonagemController {

    @Autowired
    PersonagemService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Personagem> todosPersonagens() {
        return service.todosPersonagens();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Personagem novoPersonagem(@RequestBody Personagem personagem) {
        return service.salvar(personagem);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Personagem editarPersonagem(@PathVariable Integer id, @RequestBody Personagem personagem){
        return service.editar(id, personagem);
    }
}
