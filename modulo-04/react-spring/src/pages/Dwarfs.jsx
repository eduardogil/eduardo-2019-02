import React, {Component} from 'react';
import * as axios from 'axios';

export default class Dwarfs extends Component{
    constructor ( props ) {
        super( props )
        this.state = {
            ListaDwarfs:[],
            Dwarfs:[]
        }
    }
    componentDidMount() {
        const header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:8080/api/dwarf/', header)
        .then(resp=>
            {   console.log(resp)
                const dwarf = resp.data;
                this.setState({
                    ListaDwarfs : dwarf
                })
            })
    }
    render(){
        const { ListaDwarfs } = this.state
        return(
            <React.Fragment>   
                { ListaDwarfs ? ListaDwarfs.map( dwarf => {
                    return(
                        <React.Fragment>
                        <div clasName="container">    
                            <a className="card col box">
                            <h4>ID: {dwarf.id}</h4>
                            <h3>Nome: {dwarf.nome}</h3>
                            <h4>Dano: {dwarf.dano}</h4>
                            <h4>Vida: {dwarf.vida}</h4>
                            <h4>Experiência: {dwarf.experiencia}</h4>
                            </a>
                        </div>
                        </React.Fragment>
                    )
                })
                :
                <div>
                    <h1></h1>
                </div>
            }
            
            </React.Fragment>
        )
    }
}