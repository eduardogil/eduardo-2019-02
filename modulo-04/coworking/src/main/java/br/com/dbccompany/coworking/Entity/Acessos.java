package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Acessos extends AbstractEntity{
    @Id
    @Column(name = "ID_ACESSOS")
    @SequenceGenerator(allocationSize = 1, name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
    @GeneratedValue(generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "DATA_ENTRADA")
    private Date dataEntrada;

    @Column(name = "DATA_SAIDA")
    private Date dataSaida;

    @Column(name = "IS_ENTRADA")
    private Boolean isEntrada;

    @Column(name = "IS_EXCECAO")
    private Boolean isExcecao;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumns({
            @JoinColumn(name = "ID_ESPACO", insertable = false, updatable = false, referencedColumnName = "ID_ESPACOS" ),
            @JoinColumn(name = "ID_CLIENTE", insertable = false, updatable = false, referencedColumnName = "ID_CLIENTES")})
            private SaldoCliente saldoCliente;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Date getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(Date dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public Date getDataSaida() {
        return dataSaida;
    }

    public void setDataSaida(Date dataSaida) {
        this.dataSaida = dataSaida;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Boolean getExcecao() {
        return isExcecao;
    }

    public void setExcecao(Boolean excecao) {
        isExcecao = excecao;
    }
}
