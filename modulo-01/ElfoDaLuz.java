public class ElfoDaLuz extends Elfo
{
    private int contadorAtaques = 1;
    private int QNTD_VIDA_GANHA = 10;
    private int QNTD_DANO = 21;
    private int contador=1;
    
    public ElfoDaLuz(String nome)
    {
        super(nome);
        this.ganharItem(new Item(1, "Espada de Galvorn"));
    }    

    @Override
    public void perderItem(Item item){
        if(!item.getDescricao().equals("Espada de Galvorn")){
            super.perderItem(item);
        }
    }    

    private boolean isContadorPar(){
        return this.contadorAtaques%2 == 0;    
    }

    private void resetContadorAtaq(){
        this.contadorAtaques = 0;
    }

    private void contarAtaque(){
        if(isContadorPar()){
            contadorAtaques++;
        }
        this.resetContadorAtaq();
    }
    
    private void efetuarAtaqueComEspada(Dwarf dwarf){
        this.contarAtaque();
        dwarf.sofrerDano();
        this.aumentarXp();
    }

     public int contadorDeAtaques(){
        return contador+1;
    }

            public void atacarComEspada(Dwarf dwarf){
        if(contador%2==0){
            dwarf.sofrerDano();
            this.aumentarXp();
            this.vida=this.vida+10;
            contador++;
        }else{ 
            dwarf.sofrerDano();
            this.aumentarXp();
            this.vida=this.vida-21;
            this.status=Status.SOFREU_DANO;
            contador++;
        }
            
        if(this.vida<=0){
            this.vida=0.0;
            matarPersonagem();
        }
    }
}
