import React, { Component } from 'react';
import * as axios from 'axios';
import './css/App.css';
import ListaEpisodios from './models/ListaEpisodios';
import EpisodioPadrao from './components/EpisodioPadrao'
import { Link } from 'react-router-dom';
import "./css/footer.css"
import MensagemFlash from './components/MensagemFlash';
import MensagemFlashConst from './components/MensagemFlashConst';
import MeuInputNumero from './components/MeuInputNumero';
import "./css/mirror.css";
import button from './css/button.css';
import ListaDeAvaliacoes from './components/ListaDeAvaliacoes';
import TodosEpisodios from './components/TodosEpisodios';

class BlackMirror extends Component {
  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    this.sortear = this.sortear.bind( this )
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false,
      mensagem: '',
      deveExibirErro: false
    }
  }

  
  componentDidMount(){
    axios.get().then( response => console.log(response) )   
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  marcarComoAssistido(){
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState({
      episodio
    })
  }

  alterarEstado(){
    this.setState( {
      exibirMensagem: false
      })
  }

  registrarNota( { nota, erro }){
    this.setState({
      deveExibirErro: erro
    })
    if( erro ){
      return;
    }

    const { episodio } = this.state
    let cor, mensagem
    if( episodio.validarNota( nota )){
      episodio.avaliar( nota )
      cor = 'verde'
      mensagem = MensagemFlashConst.SUCESSO.REGISTRAR_NOTA
    }else{
       cor = 'vermelho'
       mensagem = MensagemFlashConst.ERRO.NOTA_INVALIDA
      }
    this.exibirMensagem({ cor, mensagem })
  }

  exibirMensagem = ({cor, mensagem}) => {
    this.setState({
      cor,
      mensagem,
      exibirMensagem: true
    })
  }

  atualizarMensagem = devoExibir =>{
    this.setState({
      exibirMensagem: devoExibir
    })
  }
  gerarCampoNota(){
  // https://reactjs.org/docs/conditional-rendering.html
    return(
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Qual sua nota para esse episódio?</span>
              <input type="number" placeholder="1 a 5" onBlur={ this.registrarNota.bind( this )}></input>
            </div>
          )
        }
      </div>
    )
  }

  exibirMensagem = ({cor, mensagem}) => {
    this.setState({
      cor,
      mensagem,
      exibirMensagem: true
    })
  }

  atualizarMensagem = devoExibir =>{
    this.setState({
      exibirMensagem: devoExibir
    })
  }
  
  pararDeExibir(){
    this.setState( {
      exibirMensagem: false
    })
  }

  logout(){
    localStorage.removeItem('Authorization');
  }

  render() {
    const{ episodio, exibirMensagem, notaValida, cor, mensagem, deveExibirErro } = this.state

    return (
      <div className="App backMirror">
        
        <div className="header">
          <a>Black Mirror</a>
          <MensagemFlash atualizarMensagem = { this.atualizarMensagem} 
                                
                                animacao = { true } 
                                deveExibirMensagem = { exibirMensagem } 
                                cor = { cor } segundos = { 5 }
                                mensagem = {mensagem}>
          </MensagemFlash>
        </div>
        <div>
          <div className="cards">
          <EpisodioPadrao episodio = { episodio } sortearNoComp = { this.sortear.bind( this ) } marcarNoComp = {this.marcarComoAssistido.bind( this )}></EpisodioPadrao>
          <MeuInputNumero placeholder= "1 a 5"
                                  mensagemCampo="Qual sua nota para esse episodio?"
                                  obrigatorio={ true}
                                  atualizarValor={ this.registrarNota}
                                  deveExibirErro={ deveExibirErro}
                                  visivel={ episodio.assistido || false }>
          </MeuInputNumero>
          {/* <MensagemFlash exibirMensagem = { exibirMensagem } notaValida = { notaValida } alterarEstado = {this.alterarEstado.bind( this )}/>
         */}
         </div>
         </div>
        <footer>
          <Link className="link" to="/">Home</Link>
        </footer>
      </div>
    );
  }
}
/* 
cont Menu = () =>
  <section */

export default BlackMirror;


  /* 
  <MensagemFlashExemplo parar = {this.pararDeExibir.bind( this )} animacao = { true } deveExibirMensagem = { exibirMensagem } cor = { 'Verde' } segundos = { 5 } mensagem></MensagemFlashExemplo> */