import * as axios from 'axios';

export default class BuscarApi {
    constructor( ){
        this.obj = []
        this.baseUrl = 'http://localhost:8080'
    }

    buscarApi(url,id){
        url += id ? `${id}` : ''
        axios.get( `${this.baseUrl}/${url}`,{
            headers: {
                authorization: localStorage.getItem('Authorization')
            }
        })
        .then( resp => {
            this.obj = resp.data
        }).catch(function (error){
        })
    }

    buscarListaElfos(){
        this.buscarApi('api/elfo/')
    }
    buscarListaDwarfs(){
        this.buscarApi('api/dwarf/')
    }

    get buscarListaElfos() {
        return this.obj.elfos
    }
    get buscarAgencias(){
        return this.obj.agencias
    }
    get buscarClientes(){
        return this.obj.clientes
    }
    get buscarTiposDeContas(){
        return this.obj.tipos
    }
    get buscarTodasContasDeClientes(){
        return this.obj.cliente_x_conta
    }
}