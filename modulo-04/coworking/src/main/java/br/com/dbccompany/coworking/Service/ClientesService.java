package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientesService {

    @Autowired
    private ClientesRepository clientesRepository;

    @Transactional( rollbackFor = Exception.class )
    public Clientes salvar(Clientes clientes ) {
        if(clientes.getContatos().getTipoContato().getNome() == "EMAIL" ||
                clientes.getContatos().getTipoContato().getNome() == "TELEFONE" ){
            return clientesRepository.save( clientes );
        }
        return null;
    }

    @Transactional( rollbackFor = Exception.class )
    public Clientes editar( Integer id, Clientes clientes ) {
        clientes.setId( id );
        return clientesRepository.save( clientes );
    }

    public List<Clientes> todosClientes() {
        return clientesRepository.findAll();
    }
}