import React from 'react';
import "../css/App.css";
import BotaoDoSistema from '../components/BotaoDoSistema';

const EpisodioPadrao = props => {
    const{ episodio } = props
    return (
        <React.Fragment>
            <div className="botoes">
                <BotaoDoSistema cor="verde" quandoClicar={ props.sortearNoComp }>Próximo</BotaoDoSistema>
                <BotaoDoSistema cor="azul" quandoClicar={ props.marcarNoComp }>Já Assisti!</BotaoDoSistema>
            </div>
            <div>
                <h2>{ episodio.nome }</h2>
                <img className="folder" src={ episodio.thumbUrl } alt={ episodio.nome }/>
                <h4>Temporada/Episódio: { episodio.temporadaEpisodio }</h4>
                <h4>Duração: { episodio.duracaoEmMin }</h4>
                <h4>Já Assisti? { episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido } vez(es) </h4>
                <h4>Nota Atual : { episodio.nota || 'Não Possui' }</h4>
            </div>
        </React.Fragment>
    )
}

export default EpisodioPadrao