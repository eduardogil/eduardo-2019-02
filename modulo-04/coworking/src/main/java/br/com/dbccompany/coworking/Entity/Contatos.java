package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Contatos extends AbstractEntity{
    @Id
    @Column(name = "ID_CONTATOS")
    @SequenceGenerator( allocationSize = 1, name = "CONTATOS_SEQ", sequenceName = "CONTATOS_SEQ")
    @GeneratedValue(generator = "CONTATOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "VALOR", nullable = false)
    private String valor;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_TIPOCONTATO")
    private TipoContato tipoContato;


    @OneToMany(mappedBy = "contatos", cascade = CascadeType.MERGE)
    private List<Clientes> clientes = new ArrayList<>();

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public void setCliente(Clientes cliente) {
        this.clientes = clientes;
    }

    public List<Clientes> getClientes() {
        return clientes;
    }

    public void setClientes(List<Clientes> clientes) {
        this.clientes = clientes;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }


    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
