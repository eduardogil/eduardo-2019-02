package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Enum.TipoContratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, Integer > {
    List<SaldoCliente> findAll();
    SaldoCliente findByQuantidade(Integer quantidade);
    SaldoCliente findByVencimento(Date vencimento);
    SaldoCliente findByTipoContratacao(TipoContratacao tipoContratacao);
}
