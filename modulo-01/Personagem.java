public abstract class Personagem{
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida;
    protected int experiencia;
    protected int qtdExperienciaPorAtaque;
    protected double qtdDano;
    
    {
        this.status = Status.RECEM_CRIADO;
        this.inventario = new Inventario();
        this.experiencia = 0;
        this.qtdDano= 10.0;
        this.qtdExperienciaPorAtaque = 1;
    }
    
    protected Personagem(String nome){
        this.nome = nome;
    }
    
    protected String getNome(){
        return this.nome;
    }
    
    protected void setNome (String nome){
        this.nome = nome;  
    }
    
    protected Inventario getInventario(){
        return this.inventario;
    }
    
    protected double getVida(){
        return this.vida;
    }
    //
    protected Status getStatus(){
        return this.status;
    }
    
    protected int getExperiencia(){
        return this.experiencia;  
    }
    
    protected void ganharItem(Item item){
        this.inventario.adicionar(item);
    }
    
    protected void perderItem(Item item){
        this.inventario.remover(item);
    }
    
    protected void aumentarXp(){
        experiencia = experiencia + this.qtdExperienciaPorAtaque;
    }
    
    protected double calcularDano(){
        return qtdDano;
    }
    
    protected boolean podeSofrerDano(){
        return this.vida > 0;
    }
    
    protected Status matarPersonagem(){
        return this.status=status.MORTO;
    }
    
    protected void sofrerDano(){
        
        if(this.podeSofrerDano()){
            this.vida = this.vida >= this.calcularDano() ? this.vida - this.calcularDano() : this.vida;
            this.status = status.SOFREU_DANO;
        }
        
        if(this.vida<=0.0){
            this.status=status.MORTO;
            this.vida=0.0;
        }
    }
    
    protected abstract String imprimirResultado();
    
}
