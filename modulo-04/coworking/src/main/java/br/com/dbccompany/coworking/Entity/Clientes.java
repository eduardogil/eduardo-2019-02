package br.com.dbccompany.coworking.Entity;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Clientes extends AbstractEntity{
    @Id
    @Column(name = "ID_CLIENTES")
    @SequenceGenerator( allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "NOME", length = 100, nullable = false)
    private String nome;

    @CPF
    @Column(name = "CPF", nullable = false, unique = true)
    private String cpf;

    @Column(name = "DATA_NASCIMENTO", nullable = false)
    private Date dataNascimento;

    @OneToMany(mappedBy = "clientes", cascade = CascadeType.MERGE)
    private List<SaldoCliente> saldoCliente = new ArrayList<>();

    @OneToMany(mappedBy = "clientes", cascade = CascadeType.MERGE)
    private List<ClientesPacotes> clientesPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "clientes", cascade = CascadeType.MERGE)
    private List<Contratacao> contratacao = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "FK_ID_CONTATO")
    private Contatos contatos;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public List<SaldoCliente> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<SaldoCliente> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public List<Contratacao> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<Contratacao> contratacao) {
        this.contratacao = contratacao;
    }

    public Contatos getContatos() {
        return contatos;
    }

    public void setContatos(Contatos contatos) {
        this.contatos = contatos;
    }

    public List<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<Contratacao> getContratacoes() {
        return contratacao;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacao = contratacao;
    }

    public String getNome() {
        return nome;
}

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}