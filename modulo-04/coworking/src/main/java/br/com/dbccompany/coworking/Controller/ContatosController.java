package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Contatos;
import br.com.dbccompany.coworking.Service.ContatosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contatos")
public class ContatosController {

    @Autowired
    ContatosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contatos> todosContatos() {
        return service.todosContatos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Contatos novoContatos(@RequestBody Contatos contatos) {
        return service.salvar(contatos);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Contatos editarContatos(@PathVariable Integer id, @RequestBody Contatos contatos){
        return service.editar(id, contatos);
    }
}
