package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.EspacosPacotes;
import br.com.dbccompany.coworking.Repository.EspacosPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacosPacotesService {

    @Autowired
    private EspacosPacotesRepository espacosPacotesRepository;

    @Transactional( rollbackFor = Exception.class )
    public EspacosPacotes salvar(EspacosPacotes espacosPacotes ) {
        return espacosPacotesRepository.save( espacosPacotes );
    }

    @Transactional( rollbackFor = Exception.class )
    public EspacosPacotes editar( Integer id, EspacosPacotes espacosPacotes ) {
        espacosPacotes.setId( id );
        return espacosPacotesRepository.save( espacosPacotes );
    }

    public List<EspacosPacotes> todosEspacosPacotes() {
        return espacosPacotesRepository.findAll();
    }
}
