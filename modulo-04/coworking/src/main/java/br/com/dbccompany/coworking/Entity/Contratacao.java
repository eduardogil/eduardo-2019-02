package br.com.dbccompany.coworking.Entity;


import br.com.dbccompany.coworking.Enum.TipoContratacao;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Contratacao extends AbstractEntity{
    @Id
    @Column(name = "ID_CONTRATACAO")
    @SequenceGenerator(allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "QUANTIDADE")
    private Integer quantidade;

    @Column(name = "PRAZO")
    private Integer prazo;

    @Column(name = "DESCONTO")
    private Double desconto;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_ESPACO")
    private Espacos espacos;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ID_CLIENTE")
    private Clientes clientes;

    @OneToMany(mappedBy = "contratacao", cascade = CascadeType.MERGE)
    private List<Pagamentos> pagamentos = new ArrayList<>();


    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public Espacos getEspaco() {
        return espacos;
    }

    public void setEspaco(Espacos espaco) {
        this.espacos = espacos;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public Clientes getCliente() {
        return clientes;
    }

    public void setCliente(Clientes cliente) {
        this.clientes = cliente;
    }


    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }
}