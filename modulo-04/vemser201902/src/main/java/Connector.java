import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/* Arquivo de conexão com o banco de dados Oracle, verifica se a conexão é valida,
    caso contrário cria uma classe com o drive de conexão, seta os dados de conexão e executa com o get

    Erro: caso caia no catch cria um log de erro de conexão.
*/

public class Connector {

    private static Connection conn;

    public static Connection connect(){
        try {
            if(conn != null && conn.isValid(10)){
                return conn;
            }
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn = DriverManager.getConnection( "jdbc:oracle:thin:@localhost:49161:XE",
                    "SYSTEM",
                    "oracle");
        } catch(ClassNotFoundException | SQLException ex){
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO DE CONEXAO", ex);
        }
        return conn;
    }
}
