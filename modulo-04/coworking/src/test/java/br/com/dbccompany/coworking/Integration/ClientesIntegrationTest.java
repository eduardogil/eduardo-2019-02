package br.com.dbccompany.coworking.Integration;

import br.com.dbccompany.coworking.Controller.ClientesController;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class ClientesIntegrationTest {

    private MockMvc mock;

    @Autowired
    private ClientesController controller;

    @Before
    public void setUp() {
        this.mock = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testGetApiStatusOk() throws Exception {
        this.mock.perform(MockMvcRequestBuilders.get("api/clientes/todos")).andExpect(MockMvcResultMatchers.status().isOk());
    }
}