package br.com.dbccompany.bancodigital.Dto;

public class EnderecosDTO {

	private Integer idEndereco;
	private String nome;
	private Integer numero;
	private String complemento;
	
	private BairrosDTO bairros;

	public Integer getIdEndereco() {
		return idEndereco;
	}

	public void setIdEndereco(Integer idEndereco) {
		this.idEndereco = idEndereco;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public BairrosDTO getBairros() {
		return bairros;
	}

	public void setBairros(BairrosDTO bairros) {
		this.bairros = bairros;
	}
	
	
}
