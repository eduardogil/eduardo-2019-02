package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Enum.TipoContratacao;
import br.com.dbccompany.coworking.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class AcessosService {

    @Autowired
    private AcessosRepository acessosRepository;

    @Transactional( rollbackFor = Exception.class )
    public Acessos salvar(Acessos acessos ) {
        if(acessos.getEntrada() == true){
            if(acessos.getSaldoCliente().getQuantidade() > 0){
                System.out.println("Seu saldo é:"+ acessos.getSaldoCliente().getQuantidade());
            }else{
                System.out.println("Saldo insuficiente!");
            }
            if(acessos.getDataEntrada()== null){
                Date atual = new Date(System.currentTimeMillis());
                acessos.setDataEntrada(atual);
            }
        }
        if(acessos.getEntrada() == false){
            Date saida = new Date (System.currentTimeMillis());
            acessos.setDataSaida(saida);

            if(acessos.getSaldoCliente().getTipoContratacao() == TipoContratacao.MINUTOS){
            }
        }

        return acessosRepository.save( acessos );
    }

    @Transactional( rollbackFor = Exception.class )
    public Acessos editar( Integer id, Acessos acessos ) {
        acessos.setId( id );
        return acessosRepository.save( acessos );
    }

    public List<Acessos> todosAcessos() {
        return acessosRepository.findAll();
    }

    //public Acessos descontar(Acessos acessos){
    //    if()
    //}
}