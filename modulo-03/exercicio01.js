
/* exercício 1 
Crie uma função "calcularCirculo" que receba um objeto com os seguintes parâmetros:
{
    raio,  raio da circunferência
    tipoCalculo  se for "A" cálcula a área, se for "C" calcula a circunferência
}
*/

/*
let calcularCirculo = function (raio, tipoCalculo) {
    const pi = 3.14;
    if (tipoCalculo == "A") {
        return (raio * raio) * pi;
    } else if (tipoCalculo == "C") {
        return (raio*(2 * pi));
    } else {
        return 0;
    }

}

console.log(calcularCirculo(1, "A"));
console.log(calcularCirculo(12, "C"));
console.log(calcularCirculo(1, "C"));
console.log(calcularCirculo(12, "A"));

*/

function calcularCirculo({raio, tipoCalculo}){
    return Math.ceil(tipo == "A" ? Math.PI * Math.pow(raio, 2) : 2 * Math.PI * raio) ;
}

let circulo = {
    raio : 3,
    tipoCalculo : "A"
}

//console.log(calcularCirculo(circulo));

/*exercício 2
Crie uma função naoBissexto que recebe um ano (número) e verifica se ele não é bissexto. Exemplo:
naoBissexto(2016) // false
naoBissexto(2017) // true
*/
/*
let naoBissexto = function (ano) {
    if (ano % 4 == 0 && ano %400 == 0) {
        return false;
    } else if (ano % 4 == 0 && ano % 100 != 0) {
        return false;
    }
    return true;
}

console.log(naoBissexto(2016));
console.log(naoBissexto(2017));
*/

/*function naoBissexto(ano){
    if((ano %400 === 0) || (ano %4 == 0 && ano %100 !== 0) ? false : true;
}

console.log(naoBissexto(2016));
*/

let naoBissexto = ano => (ano %400===0) || (ano %4 == 0 && ano %100 !== 0) ? false : true;

/* const testes = {
    diaAula : "Segundo",
    local : "DBC",
    naoBissexto(ano){
        return (ano %400===0) || (ano %4 == 0 && ano %100 !== 0) ? false : true;
    }
}*/

console.log(naoBissexto(2016));

/* Exercício 03
Crie uma função somarPares que recebe um array de números (não precisa fazer validação) 
e soma todos números nas posições pares do array, exemplo:
somarPares( [ 1, 56, 4.34, 6, -2 ] ) // 3.34
*/

/*let somarPares = function(array){
    let soma = 0;
    for(i = 0; i < array.length; i++){
        if(i%2 == 0){
            soma = soma + array[i];
        }
    }
return soma;
}

console.log(somarPares([ 1, 56, 4.34, 6, -2 ])); //3.34
*/
function somarPares(numeros){
    let resultado = 0;
    for (let i=0; i< numeros.length; i++){
        if(i % 2 == 0){
            resultado += numeros[i];
        }
    }
    return resultado;
}

// console.log(somarPares([1, 56, 4.34, 6, -2]));

/*
let somarImpares = function(array){
    let soma = 0;
    for(i = 0; i < array.length; i++){
        if(i%2 != 0){
            soma = soma + array[i];
        }
    }
return soma;
}

console.log(somarImpares([ 1, 56, 4.34, 6, -2 ])); // 62
*/

// exercício 4

/*function adicionar(op1){
    return function(op2){
        return op1+op2;
    }
}*/

let adicionar = op1 => op2 => op1 + op2;

console.log(adicionar(3)(4)); // 7
console.log(adicionar(5642)(8749)); // 14391

/* const is_divisivel = (divisor, numero) => !(numero % divisor);
const divisor = 2;
console.log(is_divisivel(divisor, 20));
console.log(is_divisivel(divisor, 11));
console.log(is_divisivel(divisor, 12)); */

const divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);
const is_divisivel3 = divisivelPor(3);
/* console.log(is_divisivel(20));
console.log(is_divisivel(11));
console.log(is_divisivel(12));
console.log(is_divisivel3(20));
console.log(is_divisivel3(11));
console.log(is_divisivel3(12)); */

// exercício 5

function arredondar(numero, precisao = 2){
    const fator = Math.pow(10,precisao);
    return Math.ceil(numero * fator)/ fator;
}

function imprimirBRL(numero){
    let qtdCasasMilhares = 3;
    let separadorMilhar = '.';
    let separadorDecimal = ',';

    let stringBuffer = [];
    let parteDecimal = arredondar(Math.abs(numero)%1);
    let parteInteira = Math.trunc(numero);
    let parteInteiraString = Math.abs(parteInteira).toString();
    let parteInteiraTamanho = parteInteiraString.length;

    let c = 1;
    while (parteInteiraString.length > 0){
        if(c % qtdCasasMilhares == 0){
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho-c)}`)
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho-c)
        }else if(parteInteiraString.length < qtdCasasMilhares){
            stringBuffer.push(parteInteiraString)
            parteInteiraString = ''
        }
        c++;
    }
    stringBuffer.push(parteInteiraString);
    
    let decimalString = parteDecimal.toString().replace('0.','').padStart(2, '0');
    return `${parteInteira>=0 ? 'R$':'-R$'}${stringBuffer.reverse().join('') }${separadorDecimal}${decimalString}`
}

console.log(imprimirBRL(0));
console.log(imprimirBRL(3498.99));
console.log(imprimirBRL(-3498.99));
console.log(imprimirBRL(2313477.0135));