import React from 'react';
import Serie from './Serie';
import PropTypes from 'prop-types';

const SeriePadrao = props => {
    const{ serie } = props
    return (
        <React.Fragment>
            <h2>{ serie.titulo }</h2>
            <img src={ serie.thumbUrl } alt={ serie.titulo }/>
            <h4>Temporadas/Episódios: { serie.temporadas }{ serie.numeroEpisodios}</h4>
            <h4>Diretor: { serie.diretor }</h4>
            <h4>Elenco: { serie.elenco }</h4>
            <h4>Distribuidora:{ serie.distribuidora }</h4>
            
        </React.Fragment>
    )
}

export default SeriePadrao
/* 
this.titulo = titulo
        this.anoEstreia = anoEstreia
        this.diretor = diretor
        this.genero = genero
        this.elenco = elenco
        this.temporadas = temporadas
        this.numeroEpisodios = numeroEpisodios
        this.distribuidora = distribuidora */