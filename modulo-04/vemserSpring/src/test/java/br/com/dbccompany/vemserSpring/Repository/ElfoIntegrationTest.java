package br.com.dbccompany.vemserSpring.Repository;

import br.com.dbccompany.vemserSpring.Controller.ElfoController;
import br.com.dbccompany.vemserSpring.VemserSpringApplicationTests;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class ElfoIntegrationTest extends VemserSpringApplicationTests {

    private MockMvc mock;

    @Autowired
    private ElfoController controller;

    @Before
    public void setUp() {
        this.mock = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testGetApiStatusOk() throws Exception {
        this.mock.perform(MockMvcRequestBuilders.get("api/elfo/")).andExpect(MockMvcResultMatchers.status().isOk());
    }
}
