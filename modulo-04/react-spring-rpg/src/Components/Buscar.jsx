import React, { Component } from 'react';

export default class Buscar extends Component {
  constructor(props) {
    super(props)
    this.props = props
  }
  render() {
    const { buscar } = this.props
    return (
      <React.Fragment>
        <div className="inputBuscar">
          <input type="text" className="buscar" placeholder="nome ou código"onChange={buscar}></input>
        </div>
      </React.Fragment>
    )
  }
}