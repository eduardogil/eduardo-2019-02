package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacos")
public class EspacosController {

    @Autowired
    EspacosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Espacos> todosEspacos() {
        return service.todosEspacos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Espacos novoEspacos(@RequestBody Espacos espacos) {
        return service.salvar(espacos);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Espacos editarEspacos(@PathVariable Integer id, @RequestBody Espacos espacos){
        return service.editar(id, espacos);
    }
}
