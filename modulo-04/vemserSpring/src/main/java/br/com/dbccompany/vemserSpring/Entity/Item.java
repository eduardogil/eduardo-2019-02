package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Item {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ITEM_SEQ", sequenceName = "ITEM_SEQ")
    @GeneratedValue(generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ITEM")
    private Integer id;
    @Column(name = "descricao", length = 100, nullable = false)
    private String descricao;


    @OneToMany( mappedBy = "item")
    private List<InventarioXItem> lstInventarioItem;

    //Construtor
   // public Item(int quantidade, String descricao){
     //   this.descricao = descricao;
    //}

    //Getters e Setters

    public void setIdItem(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setIdItem(int id) {
        this.id = id;
    }

    public String getDescricao(){
        return this.descricao;
    }

    public void setDescricao(String descricao){
        this.descricao= descricao;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<InventarioXItem> getLstInventarioItem() {
        return lstInventarioItem;
    }

    public void setLstInventarioItem(List<InventarioXItem> lstInventarioItem) {
        this.lstInventarioItem = lstInventarioItem;
    }
}
