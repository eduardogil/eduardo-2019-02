package br.com.dbccompany.bancodigital.Dto;

public class ClientesDTO {

	private Integer idCliente;
	private String conjuge;
	private String cpf;
	private String dataNascimento;
	private String nome;
	private String rg;
	private String estadoCivil;
	
	private EnderecosDTO enderecos;

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public String getConjuge() {
		return conjuge;
	}

	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public EnderecosDTO getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(EnderecosDTO enderecos) {
		this.enderecos = enderecos;
	}
	
}
