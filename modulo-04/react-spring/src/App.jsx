import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { PrivateRoute } from './components/PrivateRoute';

import Cabecalho from './components/Cabecalho';
import Login from './pages/Login';
import Home from './pages/Home';
import Dwarfs from './pages/Dwarfs';
import Elfos from './pages/Elfos';
import Adicionar from './pages/Adicionar';

import './App.css';


export default class App extends Component {
  render(){
    return(
      <div className="App">
        <Router>
          <Cabecalho></Cabecalho>
          <React.Fragment>
            <section>
              <Route path="/login" component = { Login } />
              <PrivateRoute path="/home" exact component = { Home } />
              <PrivateRoute path="/dwarfs" exact component = { Dwarfs } />
              <PrivateRoute path="/elfos" exact component = { Elfos } />
              <PrivateRoute path="/adicionar" exact component = { Adicionar } />
            </section>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}