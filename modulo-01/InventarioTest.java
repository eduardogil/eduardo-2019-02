import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest{
    
    @Test
    public void adicionarUmItem(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1,"Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.buscar("Espada"));
    }
    
    @Test
    public void adicionarDoisItem(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1,"Espada");
        Item escudo = new Item(2,"Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(espada, inventario.buscar("Espada"));
        assertEquals(escudo, inventario.buscar("Escudo"));
    }
    
    @Test
    public void getDescricaoDosTresItens(){
        Inventario inventario = new Inventario();
        Item adaga = new Item(1,"Adaga");
        Item escudo = new Item(2,"Escudo");
        Item bracelete = new Item(2,"Bracelete");
        inventario.adicionar(adaga);
        inventario.adicionar(escudo);
        inventario.adicionar(bracelete);
        assertEquals("Adaga,Escudo,Bracelete",inventario.getDescricaoItens());
    }
    
    @Test
    public void obterItemNaPrimeiraPosicao(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1,"Espada");
        Item escudo = new Item(2,"Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(espada, inventario.obter(0));
    }
    
     @Test
    public void obterItemNaoAdicionado(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1,"Espada");
        assertNull(inventario.buscar("Espada"));
    }

    @Test
    public void removerItemAntesDeAdicionarProximo(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1,"Espada");
        Item escudo = new Item(1,"Escudo");
        inventario.adicionar(espada);
        inventario.remover(espada);
        inventario.adicionar(escudo);
        assertEquals(escudo,inventario.obter(0));
        assertEquals(1, inventario.getItens().size());
    }
    
    @Test
    public void getDescricoesVariosItens(){
      Inventario inventario = new Inventario();
      Item espada = new Item(1,"Espada");
      Item escudo = new Item(1,"Escudo");
      Item armadura = new Item(1,"Armadura");
      inventario.adicionar(espada);
      inventario.adicionar(escudo);
      inventario.adicionar(armadura);
      assertEquals("Espada,Escudo,Armadura",inventario.getDescricaoItens());
    }
    
     @Test
    public void getDescricoesNenhumItem(){
      Inventario inventario = new Inventario();
      assertEquals("",inventario.getDescricaoItens());
    }
    
    
    @Test
    public void getItemMaiorQuantidadeComVarios(){
      Inventario inventario = new Inventario();
      Item espada = new Item(5,"Espada");
      Item escudo = new Item(1,"Escudo");
      Item armadura = new Item(8,"Armadura");
      inventario.adicionar(espada);
      inventario.adicionar(escudo);
      inventario.adicionar(armadura);
      inventario.getItemComMaiorQtd();
      assertEquals(armadura,inventario.obter(2));
      assertEquals(8,inventario.obter(2).getQuantidade());
    }
    
    @Test
    public void getItemMaiorQuantidadeInventarioVazio(){
      Inventario inventario = new Inventario();
      assertNull(inventario.getItemComMaiorQtd());
    }
    
    @Test
    public void getItemMaiorQuantidadeComMesmaQuantidade(){
      Inventario inventario = new Inventario();
      Item espada = new Item(5,"Espada");
      Item escudo = new Item(5,"Escudo");
      inventario.adicionar(espada);
      inventario.adicionar(escudo);
      assertEquals(espada,inventario.getItemComMaiorQtd());
    }

    @Test
    public void buscarItemComInvetarioVazio(){
      Inventario inventario = new Inventario();
      assertNull(inventario.buscar("Capa"));;
    }
    
    @Test
    public void buscarApenasUmItem(){
      Inventario inventario = new Inventario();
      Item termica = new Item(1,"Térmica de café");
      inventario.adicionar(termica);
      assertEquals(termica, inventario.buscar("Térmica de café"));;
    }
    
    
    @Test
    public void buscarApenasUmItemComMesmaDescricao(){
      Inventario inventario = new Inventario();
      Item termica1 = new Item(1,"Térmica de café");
      Item termica2 = new Item(2,"Térmica de café");
      inventario.adicionar(termica1);
      inventario.adicionar(termica2);
      assertEquals(termica1, inventario.buscar("Térmica de café"));;
    }

    @Test
    public void inverterInventarioVazio(){
      Inventario inventario = new Inventario();
      assertTrue(inventario.inverterInventario().isEmpty());
    }
    
    @Test
    public void inverterComApenasUmItem(){
      Inventario inventario = new Inventario();
      Item termica = new Item(1,"Térmica de café");
      inventario.adicionar(termica);
      assertEquals(termica, inventario.inverterInventario().get(0));
      assertEquals(1, inventario.inverterInventario().size());
      
    }
    
    @Test
    public void inverterComDoisItens(){
      Inventario inventario = new Inventario();
      Item termica = new Item(1,"Térmica de café");
      Item caneca = new Item(2,"Caneca");
      inventario.adicionar(termica);
      inventario.adicionar(caneca);
      assertEquals(caneca, inventario.inverterInventario().get(0));
      assertEquals(2, inventario.inverterInventario().size());
      
    }
}

