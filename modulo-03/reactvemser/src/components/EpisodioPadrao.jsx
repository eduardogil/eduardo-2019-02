import React from 'react';

const EpisodioPadrao = props => {
    const{ episodio } = props
    return (
        <React.Fragment>
            <h2 class="background">{ episodio.nome }</h2>
            <img src={ episodio.thumbUrl } alt={ episodio.nome }/>
            <h4>Temporada/Episódio: { episodio.temporadaEpisodio }</h4>
            <h4>Duração: { episodio.duracaoEmMin }</h4>
            <h4>Já Assisti? { episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido } vez(es) </h4>
            <h4>{ episodio.nota || 'Sem Nota' }</h4>
            
            <button class="button button-blue" onClick={ props.sortearNoComp }>Próximo</button>
            <button class="button button-orange" onClick={() => props.marcarNoComp() }>Já Assisti!</button>
           
        </React.Fragment>
    )
}

export default EpisodioPadrao