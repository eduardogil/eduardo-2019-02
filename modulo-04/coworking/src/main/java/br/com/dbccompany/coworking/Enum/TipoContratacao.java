package br.com.dbccompany.coworking.Enum;

public enum TipoContratacao {
    MINUTOS, HORAS, TURNOS, DIARIAS, SEMANAS, MESES, ESPECIAL
}
