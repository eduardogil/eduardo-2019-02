package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Contatos;
import br.com.dbccompany.coworking.Repository.ContatosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContatosService {

    @Autowired
    private ContatosRepository contatosRepository;

    @Transactional( rollbackFor = Exception.class )
    public Contatos salvar(Contatos contatos ) {
        return contatosRepository.save( contatos );
    }

    @Transactional( rollbackFor = Exception.class )
    public Contatos editar( Integer id, Contatos contatos ) {
        contatos.setId( id );
        return contatosRepository.save( contatos );
    }

    public List<Contatos> todosContatos() {
        return contatosRepository.findAll();
    }
}
