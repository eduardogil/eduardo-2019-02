import React, {Component} from 'react';
import BuscarApi from './../components/BuscarApi';

import "../styles/cartao.css";


export default class Clientes extends Component{
    constructor(props){
        super(props)
        this.api = new BuscarApi()
        this.state = {
            ListaClientes : this.api.buscarClientes
        }
    }
    componentWillMount(){
        this.api.buscarListaClientes()
         setTimeout(() => {
            this.setState({
                ListaClientes: this.api.buscarClientes
            })
        }, 1000);
    }

    render(){
        const {ListaClientes} = this.state
        return(
            <React.Fragment>   
                {ListaClientes ? ListaClientes.map( clientes => {
                    return(
                        <React.Fragment>
                                <div>
                                    <a className="card">
                                        <div class="visa">VISA</div>
                                            <h1 className="cliente">{clientes.nome}</h1>
                                            <p className="number">{clientes.cpf}</p>
                                            <p className="id">ID: {clientes.id}</p>
                                            <div class="chip"></div>
                                        </a>
                                    </div>
                        </React.Fragment>
                    )
                })
                :
                <div>
                    <h1></h1>
                </div>
            }
            
            </React.Fragment>
        )
    }
}