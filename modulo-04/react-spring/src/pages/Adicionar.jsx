import React, {Component} from 'react';
import * as axios from 'axios';

export default class Adicionar extends Component{
    constructor(props){
        super(props)
        this.state = {
            nome: '',
            vida: '',
            experiencia:'',
            dano:'',
            tipoPersonagem:''
        }
        this.trocaValoresState = this.trocaValoresState.bind( this )
    }

    trocaValoresState(e){
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }
    
    adicionar(e){
        e.preventDefault();

        const {nome} = this.state
        console.log(nome)
        if (nome){
            axios.post('http://localhost:8080/api/dwarf/novo/',{
                nome: this.state.nome
            }).then( resp =>{
                console.log(resp)
                localStorage.setItem( 'Authorization', resp.headers.authorization );
                /* this.props.history.push("/home") */
                }
            )
        }
    }
    render(){
        return(
            <React.Fragment>   
            <div className="container">    
                    <input type="text" name="nome" id="nome" placeholder="Digite o nome do personagem" onChange={ this.trocaValoresState }/>
                    <button id="adicionar" onClick={ this.adicionar.bind( this ) }>Adicionar</button>
                
            </div>
            </React.Fragment>
        )
    }
}